import biuoop.DrawSurface;
import biuoop.GUI;
import gametools.Ball;
import geometry.Velocity;

import java.awt.Point;

public class BouncingBallAnimationTest {
    static final int WIDTH = 400;
    static final int HEIGHT = 400;
    static final int SLEEP_TIME = 50;
    static final int RADIUS = 30;

    /**
     * Draws animation of a bouncing ball.
     * @param start the start point of the ball
     * @param dx the velocity's dx
     * @param dy the velocity's dy
     */
    private static void drawAnimation(Point start, double dx, double dy) {
        GUI gui = new GUI("title", WIDTH, HEIGHT);
        biuoop.Sleeper sleeper = new biuoop.Sleeper();
        Ball ball = new Ball(start.getX(), start.getY(), RADIUS, java.awt.Color.BLACK);
        ball.setVelocity(dx, dy);
        while (true) {
            ball.moveOneStep(WIDTH, HEIGHT);
            DrawSurface d = gui.getDrawSurface();
            ball.drawOn(d);
            gui.show(d);
            sleeper.sleepFor(SLEEP_TIME);
        }
    }

    public static void main(String[] args) {
        Point notInScreen = new Point(5, 5);
        Point inScreen = new Point(200, 200);

        double rightAngle = 90;
        double leftAngle = 270;
        double negativeLeft = -90;
        double upAngle1 = 0;
        double upAngle2 = 360;
        double downAngle = 180;
        double normalAngle = 70;

        double normalSpeed = 10;
        double slowSpeed = 1;
        double fastSpeed = 100;

        Point testPoint = inScreen;
        Velocity v = Velocity.fromAngleAndSpeed(normalAngle, fastSpeed);
        if (testPoint.getX() - RADIUS < 0 || testPoint.getX() + RADIUS > WIDTH || testPoint.getY() - RADIUS < 0 || testPoint.getY() + RADIUS > HEIGHT) {
            throw new RuntimeException("ball must appear fully in surface");
        }
        drawAnimation(testPoint, v.getDx(), v.getDy());
    }
}
