import geometry.Line;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LineTest {
    private Line dotLine = new Line(2, 2, 2, 2);
    private Line vertical1 = new Line(2, 8, 2, 3);
    private Line vertical2 = new Line(4, 8, 4, 3);
    private Line horizontal1 = new Line(2, 2, 7, 2);
    private Line horizontal2 = new Line(2, 5, 7, 5);
    private Line overlap1 = new Line(2, 2, 7, 7);
    private Line overlap2 = new Line(0, 0, 5, 5);
    private Line notInRange1 = new Line(0, 0, 5, 5);
    private Line notInRange2 = new Line(2, 1, 5, 0);
    private Line intersectInEdges1 = new Line(0, 0, 5, 5);
    private Line intersectInEdges2 = new Line(0, 0, 4, 6);
    private Line intersectEdgeAndMiddle1 = new Line(0, 0, 2.5, 2.5);
    private Line intersectEdgeAndMiddle2 = new Line(0, 5, 5, 0);
    private Line parallel1 = new Line(0, 0, 5, 5);
    private Line parallel2 = new Line(3, 0, 8, 5);

    @Test
    void length() {
        assertEquals(0, dotLine.length());
    }

    @Test
    void intersectionWith() {
        testIntersectParallel();
        testIntersectEdge();
        testIntersectOverlap();
        testIntersectEqual();
        testOneVerticalNotIntersect();
        testNotIntersect();
        testTwoHorizontal();
        testEdgeAndMiddle();
        testTwoVerticals();
    }

    private void testTwoVerticals() {
        assertNull(vertical1.intersectionWith(vertical2));
    }


    private void testEdgeAndMiddle() {
        assertEquals(intersectEdgeAndMiddle1.end().toString(), intersectEdgeAndMiddle1.intersectionWith(intersectEdgeAndMiddle2).toString());
    }

    void testIntersectParallel() {
        assertNull(parallel1.intersectionWith(parallel2));
    }

    void testIntersectEdge() {
        assertEquals(intersectInEdges1.start().toString(), intersectInEdges1.intersectionWith(intersectInEdges2).toString());
    }

    void testIntersectOverlap() {
        assertNull(overlap1.intersectionWith(overlap2));
    }

    void testIntersectEqual() {
        assertNull(notInRange1.intersectionWith(notInRange1));
    }


    void testOneVerticalNotIntersect() {
        assertNull(vertical1.intersectionWith(horizontal1));
    }

    void testNotIntersect() {
        assertNull(notInRange1.intersectionWith(notInRange2));
    }

    void testTwoHorizontal() {
        assertNull(horizontal1.intersectionWith(horizontal2));
    }
}