import gamelogic.GameEnvironment;
import gametools.Ball;
import gametools.Block;
import geometry.Point;
import biuoop.DrawSurface;
import biuoop.GUI;
import biuoop.Sleeper;
import geometry.Rectangle;
import geometry.Velocity;
import interfaces.Collidable;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ass3_P1_Test {
    static final int BORDER_SIZE = 10;
    static final int WIDTH = 800;
    static final int HEIGHT = 600;
    static final int SLEEP_TIME = 20;
    static final int ANGLE = 50;
    static final int SPEED = 8;
    static final int BALL_START_X = 50;
    static final int BALL_START_Y = 50;
    static final int BALL_RADIUS = 5;
    static final int BLOCK_MARGIN = 1;
    static final Color BALL_COLOR = Color.RED;
    static final Color BLOCK_COLOR = Color.BLUE;

    private static Collidable[] createBorders(int width, int height) {
        geometry.Rectangle left = new geometry.Rectangle(new geometry.Point(0, 0), BORDER_SIZE, height);
        geometry.Rectangle right = new geometry.Rectangle(new geometry.Point(width - BORDER_SIZE, 0), BORDER_SIZE, height);
        geometry.Rectangle up = new geometry.Rectangle(new geometry.Point(0, 0), width, BORDER_SIZE);
        geometry.Rectangle down = new geometry.Rectangle(new Point(0, height - BORDER_SIZE), width, BORDER_SIZE);
        Block leftBorder = new Block(left, Color.BLACK, BLOCK_MARGIN);
        Block rightBorder = new Block(right, Color.BLACK, BLOCK_MARGIN);
        Block upBorder = new Block(up, Color.BLACK, BLOCK_MARGIN);
        Block downBorder = new Block(down, Color.BLACK, BLOCK_MARGIN);
        Collidable[] borders = new Collidable[4];
        borders[0] = leftBorder;
        borders[1] = rightBorder;
        borders[2] = upBorder;
        borders[3] = downBorder;
        return borders;
    }

    private static Collidable[] createBlocks(int numOfBlocks) {
        Scanner scanner = new Scanner(System.in);
        Collidable[] blocks = new Collidable[numOfBlocks];
        for (int i = 0; i < numOfBlocks; i++) {
            System.out.println("Enter upper-left point X coordinate:");
            double x = scanner.nextDouble();
            System.out.println("Enter upper-left point Y coordinate:");
            double y = scanner.nextDouble();
            geometry.Point upperLeft = new geometry.Point(x, y);
            System.out.println("Enter width:");
            double width = scanner.nextDouble();
            System.out.println("Enter height:");
            double height = scanner.nextDouble();
            blocks[i] = new Block(new Rectangle(upperLeft, width, height), BLOCK_COLOR, BLOCK_MARGIN);
        }
        return blocks;
    }

    private static GameEnvironment createGE(Collidable[] borders, Collidable[] blocks) {
        List<Collidable> collidables = new ArrayList<Collidable>();
        collidables.addAll(Arrays.asList(borders));
        collidables.addAll(Arrays.asList(blocks));
        return new GameEnvironment(collidables);
    }

    private static void drawAnimation(int numOfBlocks) {
        GUI gui = new GUI("Ass3_P1_Test", WIDTH, HEIGHT);
        Sleeper sleeper = new Sleeper();
        Collidable[] borders = createBorders(WIDTH, HEIGHT);
        Collidable[] blocks = createBlocks(numOfBlocks);
        Ball gameBall = new Ball(new geometry.Point(BALL_START_X, BALL_START_Y), BALL_RADIUS, BALL_COLOR);
        gameBall.setGameEnvironment(createGE(borders, blocks));
        gameBall.setVelocity(Velocity.fromAngleAndSpeed(ANGLE, SPEED));
        while (true) {
            DrawSurface d = gui.getDrawSurface();
            if (!gameBall.getGameEnvironment().getCollidables().isEmpty()) {
                for (Collidable c : gameBall.getGameEnvironment().getCollidables()) {
                    ((Block) c).drawOn(d);
                }
            }
            gameBall.drawOn(d);
            gameBall.moveOneStep();
            sleeper.sleepFor(SLEEP_TIME);
            gui.show(d);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter num of blocks:");
        int numOfBlocks = scanner.nextInt();
        drawAnimation(numOfBlocks);
    }
}
