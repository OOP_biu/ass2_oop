import geometry.Line;
import geometry.Point;
import geometry.Rectangle;

public class RectangleTest {
    public static void main(String[] args) {
        boolean flag = true;
        Rectangle rectangle = new Rectangle(new Point(5, 5), 20, 10);
        Line noIntersection = new Line(new Point(3, 3), new Point(10, 4));
        Line cornerIntersection = new Line(new Point(0, 0), new Point(7, 7));
        Line twoIntersections = new Line(new Point(0, 3), new Point(15, 30));
        Line infinityIntersections = new Line(new Point(0, 5), new Point(10, 5));
        if (!rectangle.intersectionPoints(noIntersection).isEmpty()) {
            System.out.println("FAILED - No intersection");
            flag = false;
        }
        if (rectangle.intersectionPoints(cornerIntersection).size() != 1) {
            System.out.println("FAILED - Corner intersection");
            flag = false;
        }
        if (rectangle.intersectionPoints(twoIntersections).size() != 2) {
            System.out.println("FAILED - Two intersections");
            flag = false;
        }
        if (!rectangle.intersectionPoints(infinityIntersections).isEmpty()) {
            System.out.println("FAILED - Infinity intersection");
            flag = false;
        }
        if (flag) {
            System.out.println("PASSED");
        }
    }
}
