// 313285942

package listeners;

import gamelogic.Counter;
import gametools.Ball;
import gametools.Block;
import interfaces.HitListener;

/**
 * The type Score tracking listener - responsible for tracking the points of the game.
 */
public class ScoreTrackingListener implements HitListener {
    private static final int HIT_POINTS = 5;
    private Counter currentScore;

    /**
     * Instantiates a new Score tracking listener.
     *
     * @param currentScore the current score
     */
    public ScoreTrackingListener(Counter currentScore) {
        this.currentScore = currentScore;
    }

    @Override
    public void hitEvent(Block beingHit, Ball hitter) {
        this.currentScore.increase(HIT_POINTS);
    }
}
