// 313285942

package listeners;

import gametools.Ball;
import gametools.Block;
import interfaces.HitListener;

/**
 * The type Printing hit listener.
 */
public class PrintingHitListener implements HitListener {
    /**
     * Instantiates a new Printing hit listener.
     */
    public PrintingHitListener() {
    }
    /**
     * a hit event.
     * @param beingHit the being hit
     * @param hitter   the hitter
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        System.out.println("A Block was hit.");
    }
}
