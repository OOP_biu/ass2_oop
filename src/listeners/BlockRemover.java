// 313285942

package listeners;

import gamelogic.Counter;
import levels.GameLevel;
import gametools.Ball;
import gametools.Block;
import interfaces.HitListener;

/**
 * The type Block remover - responsible for removing blocks from game.
 */
public class BlockRemover implements HitListener {
    private GameLevel gameLevel;
    private Counter remainingBlocks;

    /**
     * Instantiates a new Block remover.
     *
     * @param gameLevel            the game
     * @param remainingBlocks the remaining blocks
     */
    public BlockRemover(GameLevel gameLevel, Counter remainingBlocks) {
        this.gameLevel = gameLevel;
        this.remainingBlocks = remainingBlocks;
    }

    @Override
    public void hitEvent(Block beingHit, Ball hitter) {
        beingHit.removeFromGame(gameLevel);
        this.remainingBlocks.decrease(1);
    }
}
