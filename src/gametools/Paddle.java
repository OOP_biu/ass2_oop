// 313285942
package gametools;

import geometry.Velocity;
import geometry.Line;
import geometry.Point;
import geometry.Rectangle;
import levels.GameLevel;
import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import interfaces.Collidable;
import interfaces.Sprite;

import java.awt.Color;

/**
 * The type gametools.Paddle.
 */
public class Paddle implements Sprite, Collidable {
    //private static final double PADDLE_SPEED = 10;
    private static final int REGION_1 = 1;
    private static final int REGION_2 = 2;
    private static final int REGION_3 = 3;
    private static final int REGION_4 = 4;
    private static final int REGION_5 = 5;
    private static final int REGION_1_ANGLE = 300;
    private static final int REGION_2_ANGLE = 330;
    private static final int REGION_4_ANGLE = 30;
    private static final int REGION_5_ANGLE = 60;
    private static final double NUM_OF_REGIONS = 5;

    private KeyboardSensor keyboard;
    private Rectangle paddle;
    private Color color;
    private double surfaceWidth;
    private double borderSize;
    private double speed;

    /**
     * Instantiates a new gametools.Paddle.
     *
     * @param keyboard   the keyboard
     * @param surfaceWidth the width of the screen.
     * @param borderSize the border size of the screen.
     * @param color color of paddle.
     * @param paddle the paddle.
     * @param speed the speed of the paddle.
     */
    public Paddle(KeyboardSensor keyboard, Rectangle paddle, double surfaceWidth,
                  double borderSize, Color color, double speed) {
        this.keyboard = keyboard;
        this.surfaceWidth = surfaceWidth;
        this.borderSize = borderSize;
        this.color = color;
        this.paddle = paddle;
        this.speed = speed;
    }

    /**
     * Gets paddle.
     *
     * @return the paddle
     */
    public Rectangle getPaddle() {
        return paddle;
    }

    /**
     * Move the paddle left.
     */
    public void moveLeft() {
        if (this.paddle.getUpperLeft().getX() > this.borderSize) {
            double x = this.paddle.getUpperLeft().getX() - this.speed;
            double y = this.paddle.getUpperLeft().getY();
            this.paddle.setUpperLeft(new Point(x, y));
        }
    }

    /**
     * Move the paddle right.
     */
    public void moveRight() {
        if (this.paddle.getUpperLeft().getX() + this.paddle.getWidth() < this.surfaceWidth - this.borderSize) {
            double x = this.paddle.getUpperLeft().getX() + this.speed;
            double y = this.paddle.getUpperLeft().getY();
            this.paddle.setUpperLeft(new Point(x, y));
        }
    }

    /**
     * Add paddle to game.
     *
     * @param gameLevel the game
     */
    public void addToGame(GameLevel gameLevel) {
        Sprite s = this;
        gameLevel.addSprite(s);
        Collidable c = this;
        gameLevel.addCollidable(c);
    }

    // interfaces.Collidable methods
    @Override
    public Rectangle getCollisionRectangle() {
        return this.paddle;
    }

    @Override
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        double afterDX = currentVelocity.getDx();
        double afterDY = currentVelocity.getDy();
        Line[] edges = this.paddle.createEdges();
        if (currentVelocity.getDy() >= 0 && edges[Rectangle.UP_EDGE].isPointInLine(collisionPoint)) {
            return velocityByRegion(collisionPoint, currentVelocity);
        }
        if (currentVelocity.getDx() >= 0 && edges[Rectangle.LEFT_EDGE].isPointInLine(collisionPoint)) {
            afterDX *= -1;
        } else if (currentVelocity.getDx() < 0 && edges[Rectangle.RIGHT_EDGE].isPointInLine(collisionPoint)) {
            afterDX *= -1;
        }
        return new Velocity(afterDX, afterDY);
    }

    // interfaces.Sprite methods
    @Override
    public void drawOn(DrawSurface d) {
        d.setColor(this.color);
        Point p = this.paddle.getUpperLeft();
        d.fillRectangle((int) p.getX(), (int) p.getY(), (int) this.paddle.getWidth(), (int) this.paddle.getHeight());
    }
    @Override
    public void timePassed() {
        // TODO maybe not do else
        if (this.keyboard.isPressed(KeyboardSensor.LEFT_KEY)) {
            this.moveLeft();
        } else if (this.keyboard.isPressed(KeyboardSensor.RIGHT_KEY)) {
            this.moveRight();
        }
    }
    /**
     * The function calculates the velocity needed after hitting the paddle.
     * @param collisionPoint the point where it hit the paddle
     * @param currentVelocity the current velocity of the ball
     * @return the velocity after hit
     */
    private Velocity velocityByRegion(Point collisionPoint, Velocity currentVelocity) {
        int region = findRegion(collisionPoint);
        double newSpeed = Math.sqrt(Math.pow(currentVelocity.getDx(), 2) + Math.pow(currentVelocity.getDy(), 2));
        switch (region) {
            case REGION_1:
                return Velocity.fromAngleAndSpeed(REGION_1_ANGLE, newSpeed);
            case REGION_2:
                return Velocity.fromAngleAndSpeed(REGION_2_ANGLE, newSpeed);
            case REGION_3:
                return new Velocity(currentVelocity.getDx(), -currentVelocity.getDy());
            case REGION_4:
                return Velocity.fromAngleAndSpeed(REGION_4_ANGLE, newSpeed);
            case REGION_5:
                return Velocity.fromAngleAndSpeed(REGION_5_ANGLE, newSpeed);
            default:
                return null;
        }
    }

    /**
     * Finds out in which region was the hit of the ball.
     * @param collisionPoint the point were it hit the paddle/
     * @return the region.
     */
    private int findRegion(Point collisionPoint) {
        double regionSize = this.paddle.getWidth() / NUM_OF_REGIONS;
        // the x coordinates here are calculated to be the start of the region
        double start = this.paddle.getUpperLeft().getX();
        double region1 = start + regionSize;
        double region2 = region1 + regionSize;
        double region3 = region2 + regionSize;
        double region4 = region3 + regionSize;
        double region5 = region4 + regionSize;
        if (collisionPoint.getX() >= start && collisionPoint.getX() <= region1) {
            return REGION_1;
        }
        if (collisionPoint.getX() >= region1 && collisionPoint.getX() <= region2) {
            return REGION_2;
        }
        if (collisionPoint.getX() >= region2 && collisionPoint.getX() <= region3) {
            return REGION_3;
        }
        if (collisionPoint.getX() >= region3 && collisionPoint.getX() <= region4) {
            return REGION_4;
        }
        if (collisionPoint.getX() >= region4 && collisionPoint.getX() <= region5) {
            return REGION_5;
        }
        // default region
        return REGION_3;
    }
}
