package gametools;

import biuoop.DrawSurface;
import levels.GameLevel;
import interfaces.Sprite;

import java.awt.Color;

/**
 * The type Level name indicator.
 */
public class LevelNameIndicator implements Sprite {
    private String name;
    private int x;
    private int y;
    private int fontSize;
    private Color color;

    /**
     * Instantiates a new Level name indicator.
     *
     * @param name     the name
     * @param x        the x
     * @param y        the y
     * @param fontSize the font size
     * @param color    the color
     */
    public LevelNameIndicator(String name, int x, int y, int fontSize, Color color) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.fontSize = fontSize;
        this.color = color;
    }

    @Override
    public void drawOn(DrawSurface d) {
        d.setColor(this.color);
        d.drawText(this.x, this.y, this.name, this.fontSize);
    }

    @Override
    public void timePassed() {
    }

    /**
     * Add to game.
     *
     * @param gameLevel the game level
     */
    public void addToGame(GameLevel gameLevel) {
        Sprite s = this;
        gameLevel.addSprite(s);
    }
}
