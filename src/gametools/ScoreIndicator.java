// 313285942

package gametools;

import biuoop.DrawSurface;
import gamelogic.Counter;
import levels.GameLevel;
import interfaces.Sprite;

import java.awt.Color;

/**
 * The type Score indicator - responsible for displaying score on surface.
 */
public class ScoreIndicator implements Sprite {
    private Counter score;
    private int x;
    private int y;
    private int fontSize;
    private Color color;

    /**
     * Instantiates a new Score indicator.
     *
     * @param score    the score
     * @param x        the x
     * @param y        the y
     * @param fontSize the font size
     * @param color the color
     */
    public ScoreIndicator(Counter score, int x, int y, int fontSize, Color color) {
        this.score = score;
        this.x = x;
        this.y = y;
        this.fontSize = fontSize;
        this.color = color;
    }

    @Override
    public void drawOn(DrawSurface d) {
        d.setColor(color);
        d.drawText(this.x, this.y, "Score: " + this.score.getValue(), this.fontSize);
    }

    @Override
    public void timePassed() {
    }

    /**
     * Add to game.
     *
     * @param gameLevel the game
     */
    public void addToGame(GameLevel gameLevel) {
       Sprite s = this;
       gameLevel.addSprite(s);
    }
}
