// 313285942
package gametools;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import levels.GameLevel;
import geometry.Line;
import geometry.Point;
import biuoop.DrawSurface;
import geometry.Rectangle;
import geometry.Velocity;
import interfaces.Collidable;
import interfaces.HitListener;
import interfaces.HitNotifier;
import interfaces.Sprite;

/**
 * The type gametools.Block.
 * The class implements the interfaces.Collidable and interfaces.Sprite interfaces.
 * The class uses the geometry.Rectangle class.
 */
public class Block implements Collidable, Sprite, HitNotifier {
    private int margin;
    private Rectangle block;
    private Color color;
    private List<HitListener> hitListeners;

    /**
     * Instantiates a new gametools.Block.
     *
     * @param block the block
     * @param color the color
     * @param margin the size of the border around the block.
     */
    public Block(Rectangle block, Color color, int margin) {
        this.block = new Rectangle(block.getUpperLeft(), block.getWidth(), block.getHeight());
        this.color = color;
        this.margin = margin;
        this.hitListeners = new ArrayList<>();
    }
    // Collidable interface methods
    @Override
    public Rectangle getCollisionRectangle() {
        return this.block;
    }
    @Override
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        double afterDX = currentVelocity.getDx();
        double afterDY = currentVelocity.getDy();
        Line[] edges = this.block.createEdges();
        // the ball can only change his dx if it hits the closest edge in his direction.
        if (currentVelocity.getDx() >= 0 && edges[Rectangle.LEFT_EDGE].isPointInLine(collisionPoint)) {
            afterDX *= -1;
        } else if (currentVelocity.getDx() < 0 && edges[Rectangle.RIGHT_EDGE].isPointInLine(collisionPoint)) {
            afterDX *= -1;
        }
        // the ball can only change his dx if it hits the closest edge in his direction.
        if (currentVelocity.getDy() >= 0 && edges[Rectangle.UP_EDGE].isPointInLine(collisionPoint)) {
            afterDY *= -1;
        } else if (currentVelocity.getDy() < 0 && edges[Rectangle.DOWN_EDGE].isPointInLine(collisionPoint)) {
            afterDY *= -1;
        }
        this.notifyHit(hitter);
        return new Velocity(afterDX, afterDY);
    }
    // Sprite interface methods
    @Override
    public void drawOn(DrawSurface d) {
        Point p = this.block.getUpperLeft();
        d.setColor(Color.BLACK);
        d.fillRectangle((int) p.getX() - margin, (int) p.getY() - margin,
                (int) this.block.getWidth() + margin, (int) this.block.getHeight() + margin);
        d.setColor(this.color);
        d.fillRectangle((int) p.getX(), (int) p.getY(), (int) this.block.getWidth(), (int) this.block.getHeight());
    }
    @Override
    public void timePassed() {
    }
    // HitNotifier interface methods
    @Override
    public void addHitListener(HitListener hl) {
        this.hitListeners.add(hl);
    }

    @Override
    public void removeHitListener(HitListener hl) {
        this.hitListeners.remove(hl);
    }

    // Other methods

    /**
     * Gets listeners list.
     *
     * @return the listeners
     */
    protected List<HitListener> getListeners() {
        return this.hitListeners;
    }

    /**
     * Notify all the listeners that a hit occurred.
     * @param hitter the ball that hit the block
     */
    private void notifyHit(Ball hitter) {
        List<HitListener> listeners = new ArrayList<>(this.hitListeners);
        for (HitListener hl : listeners) {
            hl.hitEvent(this, hitter);
        }
    }

    /**
     * Add block to game's collidables and sprites.
     *
     * @param gameLevel the game
     */
    public void addToGame(GameLevel gameLevel) {
        Collidable c = this;
        gameLevel.addCollidable(c);
        Sprite s = this;
        gameLevel.addSprite(s);
    }

    /**
     * Remove block from game.
     *
     * @param gameLevel the game
     */
    public void removeFromGame(GameLevel gameLevel) {
        gameLevel.removeCollidable(this);
        gameLevel.removeSprite(this);
    }

    /**
     * Gets block.
     *
     * @return the block
     */
    protected Rectangle getBlock() {
        return this.block;
    }

    /**
     * Gets color.
     *
     * @return the color
     */
    protected Color getColor() {
        return this.color;
    }
}
