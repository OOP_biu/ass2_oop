// 313285942

package gametools;

import biuoop.DrawSurface;
import geometry.Point;
import geometry.Rectangle;
import java.awt.Color;

/**
 * The type Border.
 */
public class Border extends Block {
    /**
     * Instantiates a new Border.
     *
     * @param block  the block
     * @param color  the color
     * @param margin the size of the border around the block.
     */
    public Border(Rectangle block, Color color, int margin) {
        super(block, color, margin);
    }

    @Override
    public void drawOn(DrawSurface d) {
        Rectangle border = super.getBlock();
        Point p = border.getUpperLeft();
        d.setColor(Color.BLACK);
        d.setColor(super.getColor());
        d.fillRectangle((int) p.getX(), (int) p.getY(), (int) border.getWidth(), (int) border.getHeight());
    }
}
