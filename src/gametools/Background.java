package gametools;

import biuoop.DrawSurface;
import levels.GameLevel;
import geometry.Point;
import geometry.Rectangle;
import interfaces.Sprite;

import java.awt.Color;

/**
 * The type Background.
 */
public class Background implements Sprite {
    private Color color;
    private Rectangle rectangle;
    private Sprite image;

    /**
     * Instantiates a new Background.
     *
     * @param color     the color
     * @param rectangle the rectangle
     * @param image     the image
     */
    public Background(Color color, Rectangle rectangle, Sprite image) {
        this.color = color;
        this.rectangle = rectangle;
        this.image = image;
    }

    @Override
    public void drawOn(DrawSurface d) {
        d.setColor(color);
        Point upperLeft = this.rectangle.getUpperLeft();
        d.fillRectangle((int) upperLeft.getX(), (int) upperLeft.getY(),
                (int) this.rectangle.getWidth(), (int) this.rectangle.getHeight());
        if (image != null) {
            image.drawOn(d);
        }
    }

    @Override
    public void timePassed() {
    }

    /**
     * Add background to game.
     *
     * @param gameLevel the game
     */
    public void addToGame(GameLevel gameLevel) {
        gameLevel.addSprite((Sprite) this);
    }
}
