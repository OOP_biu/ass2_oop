// 313285942
package gametools;

import java.awt.Color;
import geometry.Velocity;
import geometry.Line;
import geometry.Point;
import gamelogic.GameEnvironment;
import gamelogic.CollisionInfo;
import levels.GameLevel;
import biuoop.DrawSurface;
import interfaces.Sprite;

/**
 * The type gametools.Ball.
 * The class supports the methods - moveOneStep and drawOn.
 * The ball is implemented by center point, radius, color, velocity and game environment.
 */
public class Ball implements Sprite {
    private Point center;
    private int r;
    private Color color;
    private Velocity velocity;
    private GameEnvironment gameEnvironment;

    // Constructors
    /**
     * Instantiates a new gametools.Ball from a point, a radius and a color.
     * The default value of the velocity is (0, 0).
     *
     * @param center the point given
     * @param r      the radius
     * @param color  the color
     */
    public Ball(Point center, int r, Color color) {
        this.center = new Point(center.getX(), center.getY());
        this.r = r;
        this.color = color;
        this.velocity = new Velocity(0, 0);
        this.gameEnvironment = null;
    }

    /**
     * Instantiates a new gametools.Ball from x and y coordinates, a radius and a color.
     * The default value of the velocity is (0, 0).
     *
     * @param x     the x coordinate
     * @param y     the y coordinate
     * @param r     the radius
     * @param color the color
     */
    public Ball(double x, double y, int r, Color color) {
        this.center = new Point(x, y);
        this.r = r;
        this.color = color;
        this.velocity = new Velocity(0, 0);
        this.gameEnvironment = null;
    }

    /**
     * Instantiates a new gametools.Ball from a point, a radius, a color and velocity.
     *
     * @param center the point given
     * @param r      the radius
     * @param color  the color
     * @param velocity the velocity
     */
    public Ball(Point center, int r, Color color, Velocity velocity) {
        this.center = new Point(center.getX(), center.getY());
        this.r = r;
        this.color = color;
        this.velocity = new Velocity(velocity.getDx(), velocity.getDy());
        this.gameEnvironment = null;
    }

    /**
     * Instantiates a new gametools.Ball from x and y coordinates, a radius, a color and velocity.
     * The default value of the velocity is (0, 0).
     *
     * @param x     the x coordinate
     * @param y     the y coordinate
     * @param r     the radius
     * @param color the color
     * @param velocity the velocity
     */
    public Ball(double x, double y, int r, Color color, Velocity velocity) {
        this.center = new Point(x, y);
        this.r = r;
        this.color = color;
        this.velocity = new Velocity(velocity.getDx(), velocity.getDy());
        this.gameEnvironment = null;
    }

    /**
     * Instantiates a new gametools.Ball from a point, a radius, a color, velocity and gamemanagement.Game Environment.
     *
     * @param center the point given
     * @param r      the radius
     * @param color  the color
     * @param velocity the velocity
     * @param ge the gamemanagement.Game Environment
     */
    public Ball(Point center, int r, Color color, Velocity velocity, GameEnvironment ge) {
        this.center = new Point(center.getX(), center.getY());
        this.r = r;
        this.color = color;
        this.velocity = new Velocity(velocity.getDx(), velocity.getDy());
        this.gameEnvironment = new GameEnvironment(ge.getCollidables());
    }

    // Queries
    /**
     * Gets x coordinate of ball's center.
     *
     * @return the x coordinate
     */
    public int getX() {
        return (int) this.center.getX();
    }

    /**
     * Gets y coordinate of ball's center.
     *
     * @return the y
     */
    public int getY() {
        return (int) this.center.getY();
    }

    /**
     * Gets size (radius) of the ball.
     *
     * @return the size
     */
    public int getSize() {
        return r;
    }

    /**
     * Gets color of the ball.
     *
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Gets velocity of the ball.
     *
     * @return the color
     * @throws RuntimeException if velocity wasn't initialized.
     */
    public Velocity getVelocity() throws RuntimeException {
        if (this.velocity == null) {
            throw new RuntimeException("velocity must be initialized");
        }
        return this.velocity;
    }

    /**
     * Gets game environment.
     *
     * @return the game environment
     * @throws RuntimeException if game environment wasn't initialized.
     */
    public GameEnvironment getGameEnvironment() throws RuntimeException {
        if (this.gameEnvironment == null) {
            throw new RuntimeException("game environment must be initialized");
        }
        return this.gameEnvironment;
    }

    // Commands
    /**
     * Sets velocity.
     *
     * @param v the velocity
     */
    public void setVelocity(Velocity v) {
        this.velocity = new Velocity(v.getDx(), v.getDy());
    }

    /**
     * Sets velocity.
     *
     * @param dx the dx of velocity
     * @param dy the dy of velocity
     */
    public void setVelocity(double dx, double dy) {
        this.velocity = new Velocity(dx, dy);
    }

    /**
     * Sets game enviornment.
     *
     * @param ge the game environment
     */
    public void setGameEnvironment(GameEnvironment ge) {
        this.gameEnvironment = new GameEnvironment(ge.getCollidables());
    }

    // Sprite interface methods
    @Override
    public void drawOn(DrawSurface surface) {
        surface.setColor(Color.BLACK);
        surface.drawCircle(this.getX(), this.getY(), r);
        surface.setColor(color);
        surface.fillCircle(this.getX(), this.getY(), r);
    }
    @Override
    public void timePassed() {
        this.moveOneStep();
    }

    /**
     * Move one step according to velocity in given bounds.
     *
     * @param width  the width bound
     * @param height the height bound
     * @throws RuntimeException if velocity wasn't initialized.
     */
    public void moveOneStep(double width, double height) throws RuntimeException {
        if (this.velocity == null) {
            throw new RuntimeException("velocity must be initialized");
        }
        double dx = this.velocity.getDx();
        double dy = this.velocity.getDy();
        if (this.center.getX() - r + dx <= 0 || this.center.getX() + r + dx >= width) {
            this.velocity.setDx(-dx);
        }
        if (this.center.getY() - r + dy <= 0 || this.center.getY() + r + dy >= height) {
            this.velocity.setDy(-dy);
        }
        this.center = this.getVelocity().applyToPoint(this.center);
    }


    /**
     * Move one step according to velocity in given bounds.
     *
     * @param minX the min x
     * @param minY the min y
     * @param maxX the max x
     * @param maxY the max y
     * @throws RuntimeException if velocity wasn't initialized.
     */
    public void moveOneStep(double minX, double minY, double maxX, double maxY) throws RuntimeException {
        if (this.velocity == null) {
            throw new RuntimeException("velocity must be initialized");
        }
        double dx = this.velocity.getDx();
        double dy = this.velocity.getDy();
        if (this.center.getX() - r  + dx <= minX || this.center.getX() + r + dx >= maxX) {
            this.velocity.setDx(-dx);
        }
        if (this.center.getY() - r + dy <= minY || this.center.getY() + r + dy >= maxY) {
            this.velocity.setDy(-dy);
        }
        this.center = this.getVelocity().applyToPoint(this.center);
    }

    /**
     * Move one step according to velocity.
     * @throws RuntimeException if game environment or velocity weren't initialized.
     */
    public void moveOneStep() throws RuntimeException {
        if (this.gameEnvironment == null) {
            throw new RuntimeException("game environment must be initialized");
        }
        if (this.velocity == null) {
            throw new RuntimeException("velocity must be initialized");
        }
        double dx = this.velocity.getDx();
        double dy = this.velocity.getDy();
        Line trajectory = createTrajectory();
        CollisionInfo collisionInfo = this.gameEnvironment.getClosestCollision(trajectory);
        if (collisionInfo != null) {
            Point collisionPoint = collisionInfo.collisionPoint();
            /*
            the ball should move "almost" to hit point, that's what the epsilon for.
            because it's depended on the direction of the velocity, the epsilon is multiplied by the sign of dx and dy.
             */
            double xEpsilon = Math.signum(dx) * this.r;
            double yEpsilon = Math.signum(dy) * this.r;
            this.center = new Point(collisionPoint.getX() - xEpsilon, collisionPoint.getY() - yEpsilon);
            this.velocity = collisionInfo.collisionObject().hit(this, collisionInfo.collisionPoint(), this.velocity);
            // if collision info is null then there is no obstacles in the way of the ball.
        } else {
            this.center = this.velocity.applyToPoint(this.center);
        }
    }

    /**
     * Add to ball to game's sprites.
     *
     * @param gameLevel the game
     */
    public void addToGame(GameLevel gameLevel) {
        Sprite ball = this;
        gameLevel.addSprite(ball);
    }

    /**
     * Remove ball from game.
     *
     * @param gameLevel the game
     */
    public void removeFromGame(GameLevel gameLevel) {
        gameLevel.removeSprite(this);
    }

    /**
     * The function creates the trajectory of the ball.
     * @return the trajectory.
     */
    private Line createTrajectory() {
        double dx = this.velocity.getDx();
        double dy = this.velocity.getDy();
        Point end = new Point(this.center.getX() + dx, this.center.getY() + dy);
        return new Line(this.center, end);
    }
}
