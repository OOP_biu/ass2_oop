// 313285942

package gamelogic;

/**
 * The type Counter.
 */
public class Counter {
    private int value;

    /**
     * Instantiates a new Counter.
     *
     * @param value the value
     */
    public Counter(int value) {
        this.value = value;
    }

    /**
     * Instantiates a new Counter - default value is 0..
     */
    public Counter() {
        this.value = 0;
    }

    /**
     * Increase the value.
     *
     * @param number the number to increase
     */
    public void increase(int number) {
        this.value += number;
    }

    /**
     * Decrease the value.
     *
     * @param number the number to decrease
     */
    public void decrease(int number) {
        this.value -= number;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public int getValue() {
        return this.value;
    }
}
