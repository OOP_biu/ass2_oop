// 313285942
package gamelogic;

import biuoop.DrawSurface;
import interfaces.Sprite;

import java.util.ArrayList;
import java.util.List;

/**
 * The type interfaces.Sprite collection.
 */
public class SpriteCollection {
    private List<Sprite> sprites;
    /**
     * Instantiates a new interfaces.Sprite collection from a given list.
     *
     * @param sprites the sprites list
     */
    public SpriteCollection(List<Sprite> sprites) {
        this.sprites = sprites;
    }

    /**
     * Instantiates a new interfaces.Sprite collection.
     */
    public SpriteCollection() {
        this.sprites = new ArrayList<>();
    }

    /**
     * Add sprite to sprites collection.
     *
     * @param s the s
     */
    public void addSprite(Sprite s) {
        this.sprites.add(s);
    }

    /**
     * Notify all sprites time passed.
     */
    public void notifyAllTimePassed() {
        List<Sprite> spritesInGame = new ArrayList<>(this.sprites);
        for (Sprite s : spritesInGame) {
            s.timePassed();
        }
    }

    /**
     * Draw all sprites on surface.
     *
     * @param d the surface
     */
    public void drawAllOn(DrawSurface d) {
        for (Sprite s : this.sprites) {
            s.drawOn(d);
        }
    }

    /**
     * Remove sprite from sprites collection.
     *
     * @param s the sprite.
     */
    public void removeSprite(Sprite s) {
        this.sprites.remove(s);
    }
}
