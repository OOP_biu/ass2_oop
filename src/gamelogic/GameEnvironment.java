// 313285942
package gamelogic;

import geometry.Line;
import geometry.Point;
import interfaces.Collidable;

import java.util.ArrayList;
import java.util.List;

/**
 * The type gamemanagement.Game environment.
 * The class implements the methods - getCollidables and getClosestCollisionInfo.
 */
public class GameEnvironment {
    private List<Collidable> collidables;

    /**
     * Instantiates a new gamemanagement.Game environment from given collidables list.
     *
     * @param collidables the collidables
     */
    public GameEnvironment(List<Collidable> collidables) {
        this.collidables = collidables;
    }

    /**
     * Instantiates a new gamemanagement.Game Environment.
     */
    public GameEnvironment() {
        this.collidables = new ArrayList<Collidable>();
    }

    /**
     * Gets collidables list.
     *
     * @return the list of collidables
     */
    public List<Collidable> getCollidables() {
        return collidables;
    }

    /**
     * Gets closest collision object's info.
     *
     * @param trajectory the trajectory
     * @return the closest collision
     */
    public CollisionInfo getClosestCollision(Line trajectory) {
        List<Collidable> collided = collidedObjects(trajectory);
        // if list is empty there is no interfaces.Collidable in the trajectory
        if (collided.isEmpty()) {
            return null;
        }
        Point start = trajectory.start();
        Collidable closestObj = collided.get(0);
        double minDist;
        // finds out the closest-intersection's object
        for (Collidable c : collided) {
            minDist = trajectory.closestIntersectionToStartOfLine(closestObj.getCollisionRectangle()).distance(start);
            if (trajectory.closestIntersectionToStartOfLine(c.getCollisionRectangle()).distance(start) < minDist) {
                closestObj = c;
            }
        }
        Point closestPoint = trajectory.closestIntersectionToStartOfLine(closestObj.getCollisionRectangle());
        return new CollisionInfo(closestPoint, closestObj);
    }

    /**
     * Add collidable to the collidables list.
     *
     * @param c the c
     */
    public void addCollidable(Collidable c) {
        this.collidables.add(c);
    }

    /**
     * The function creates a list of all the objects a given line collides with.
     * @param trajectory the line
     * @return the list
     */
    private List<Collidable> collidedObjects(Line trajectory) {
        List<Collidable> collided = new ArrayList<Collidable>();
        for (Collidable c : this.collidables) {
            if (trajectory.closestIntersectionToStartOfLine(c.getCollisionRectangle()) != null) {
                collided.add(c);
            }
        }
        return collided;
    }

    /**
     * Remove collidable from collidables list.
     *
     * @param c the collidable.
     */
    public void removeCollidable(Collidable c) {
        this.collidables.remove(c);
    }
}
