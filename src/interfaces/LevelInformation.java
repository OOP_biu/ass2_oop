package interfaces;

import gametools.Block;
import geometry.Velocity;

import java.util.List;

/**
 * The interface Level information.
 */
public interface LevelInformation {
    /**
     * returns num of balls.
     *
     * @return the amount of balls.
     */
    int numOfBalls();

    /**
     * returns a list of all ball velocities.
     *
     * @return the list
     */
    List<Velocity> initialBallVelocities();

    /**
     * returns paddle's speed.
     *
     * @return the speed
     */
    int paddleSpeed();

    /**
     * returns paddle's width.
     *
     * @return the width.
     */
    int paddleWidth();

    /**
     * returns level's name.
     *
     * @return the name
     */
    String levelName();

    /**
     * Gets background.
     *
     * @return the background
     */
    Sprite getBackground();

    /**
     * Blocks list.
     *
     * @return the list
     */
    List<Block> blocks();

    /**
     * Number of blocks to remove.
     *
     * @return the num
     */
    int numberOfBlocksToRemove();
}
