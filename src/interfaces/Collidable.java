// 313285942
package interfaces;

import gametools.Ball;
import geometry.Velocity;
import geometry.Point;
import geometry.Rectangle;

/**
 * The interface interfaces.Collidable - Objects that the ball can collide with them.
 */
public interface Collidable {

    /**
     * Gets the rectangle the ball collided with.
     *
     * @return the rectangle
     */
    Rectangle getCollisionRectangle();

    /**
     * The function calculates the velocity of the ball after collision.
     *
     * @param hitter the ball that hit the collidable
     * @param collisionPoint  the collision point
     * @param currentVelocity the current velocity
     * @return the velocity after collision
     */
    Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity);
}
