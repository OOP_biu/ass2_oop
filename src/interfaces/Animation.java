package interfaces;

import biuoop.DrawSurface;

/**
 * The interface Animation.
 */
public interface Animation {
    /**
     * Do one frame.
     *
     * @param d the d
     */
    void doOneFrame(DrawSurface d);

    /**
     * Check if should stop animation.
     *
     * @return the boolean
     */
    boolean shouldStop();
}
