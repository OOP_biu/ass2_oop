package interfaces;

import gametools.Ball;
import gametools.Block;

/**
 * The interface Hit listener.
 */
public interface HitListener {
    /**
     * Notify listeners hit occurred.
     *
     * @param beingHit the being hit
     * @param hitter   the hitter
     */
    void hitEvent(Block beingHit, Ball hitter);
}
