package interfaces;

import biuoop.DrawSurface;

/**
 * The interface interfaces.Sprite.
 */
public interface Sprite {
    /**
     * Draw sprite on surface.
     *
     * @param d the surface
     */
    void drawOn(DrawSurface d);

    /**
     * Time passed.
     */
    void timePassed();
}
