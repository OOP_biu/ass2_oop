package levels;

import gametools.Background;
import gametools.Block;
import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import interfaces.Sprite;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Final four.
 */
public class FinalFour extends BasicLevel {
    private static final int NUM_OF_BALLS = 3;
    private static final int MIN_ANGLE = -50;
    private static final int MAX_ANGLE = 50;
    private static final double BALL_SPEED = 6;
    private static final int PADDLE_SPEED = 10;
    private static final int PADDLE_WIDTH = 100;
    private static final String LEVEL_NAME = "Final Four";
    private static final Color BACKGROUND_COLOR = new Color(51, 153, 255);
    private static final Color LIGHT_BLUE = new Color(51, 204, 255);
    private static final int BLOCKS_IN_ROW = 15;
    private static final int BLOCK_MARGIN = 1;
    private static final int NUM_OF_ROWS = 7;
    private static final double BLOCKS_Y = 100;
    private static final double BLOCK_HEIGHT = 20;

    /**
     * Instantiates a new Final Four level.
     *
     * @param surfaceWidth  the surface width
     * @param surfaceHeight the surface height
     * @param borderSize    the border size
     */
    public FinalFour(int surfaceWidth, int surfaceHeight, int borderSize) {
        super(surfaceWidth, surfaceHeight, borderSize);
    }

    @Override
    public int numOfBalls() {
        return NUM_OF_BALLS;
    }

    @Override
    public List<Velocity> initialBallVelocities() {
        List<Velocity> velocities = new ArrayList<>();
        for (int i = 0; i < NUM_OF_BALLS; i++) {
            // in order to keep an even angle between all balls.
            double margin = i * ((double) (MAX_ANGLE - MIN_ANGLE) / (NUM_OF_BALLS - 1));
            Velocity v = Velocity.fromAngleAndSpeed(MIN_ANGLE + margin, BALL_SPEED);
            velocities.add(v);
        }
        return velocities;
    }

    @Override
    public int paddleSpeed() {
        return PADDLE_SPEED;
    }

    @Override
    public int paddleWidth() {
        return PADDLE_WIDTH;
    }

    @Override
    public String levelName() {
        return LEVEL_NAME;
    }

    @Override
    public Sprite getBackground() {
        Rectangle rectangle = new Rectangle(new Point(0, 0), super.getSurfaceWidth(), super.getSurfaceHeight());
        return new Background(BACKGROUND_COLOR, rectangle, null);
    }

    @Override
    public List<Block> blocks() {
        List<Block> blocks = new ArrayList<>();
        Color[] colors = {Color.GRAY, Color.RED, Color.YELLOW, Color.GREEN, Color.WHITE, Color.PINK, LIGHT_BLUE};
        double borderSize = super.getBorderSize();
        double blockWidth = (super.getSurfaceWidth() - 2 * super.getBorderSize() - BLOCKS_IN_ROW * BLOCK_MARGIN)
                / (double) BLOCKS_IN_ROW;
        for (int i = 0; i < NUM_OF_ROWS; i++) {
            for (int j = 0; j < BLOCKS_IN_ROW; j++) {
                Point upperLeft = new Point(borderSize + j * (blockWidth + BLOCK_MARGIN),
                                            BLOCKS_Y + i * (BLOCK_HEIGHT + BLOCK_MARGIN));
                Rectangle rectangle = new Rectangle(upperLeft, blockWidth, BLOCK_HEIGHT);
                Block block = new Block(rectangle, colors[i % colors.length], BLOCK_MARGIN);
                blocks.add(block);
            }
        }
        return blocks;
    }

    @Override
    public int numberOfBlocksToRemove() {
        return NUM_OF_ROWS * BLOCKS_IN_ROW;
    }
}
