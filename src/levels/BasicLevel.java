package levels;

import interfaces.LevelInformation;

/**
 * The type Basic level.
 */
public abstract class BasicLevel implements LevelInformation {
    private int surfaceWidth;
    private int surfaceHeight;
    private int borderSize;

    /**
     * Instantiates a new Basic level.
     *
     * @param surfaceWidth  the surface width
     * @param surfaceHeight the surface height
     * @param borderSize    the border size
     */
    public BasicLevel(int surfaceWidth, int surfaceHeight, int borderSize) {
        this.surfaceWidth = surfaceWidth;
        this.surfaceHeight = surfaceHeight;
        this.borderSize = borderSize;
    }

    /**
     * Gets surface width.
     *
     * @return the surface width
     */
    protected int getSurfaceWidth() {
        return this.surfaceWidth;
    }

    /**
     * Gets surface height.
     *
     * @return the surface height
     */
    protected int getSurfaceHeight() {
        return this.surfaceHeight;
    }

    /**
     * Gets border size.
     *
     * @return the border size
     */
    protected int getBorderSize() {
        return this.borderSize;
    }
}
