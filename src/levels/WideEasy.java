package levels;

import gametools.Background;
import gametools.Block;
import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import interfaces.Sprite;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Wide easy.
 */
public class WideEasy extends BasicLevel {
    private static final int NUM_OF_BALLS = 10;
    private static final int BALL_SPEED = 5;
    private static final int MIN_ANGLE = -60;
    private static final int MAX_ANGLE = 60;
    private static final int PADDLE_SPEED = 5;
    private static final int SURFACE_PADDLE_DELTA = 200;
    private static final String LEVEL_NAME = "Wide Easy";
    private static final int BLOCKS_Y = 200;
    private static final int NUM_OF_BLOCKS = 15;
    private static final int BLOCK_MARGIN = 1;
    private static final double BLOCK_HEIGHT = 20;
    private static final Color BACKGROUND_COLOR = Color.WHITE;

    /**
     * Instantiates a new Wide easy level.
     *
     * @param surfaceWidth  the surface width
     * @param surfaceHeight the surface height
     * @param borderSize    the border size
     */
    public WideEasy(int surfaceWidth, int surfaceHeight, int borderSize) {
        super(surfaceWidth, surfaceHeight, borderSize);
    }


    @Override
    public int numOfBalls() {
        return NUM_OF_BALLS;
    }

    @Override
    public List<Velocity> initialBallVelocities() {
        List<Velocity> velocities = new ArrayList<>();
        for (int i = 0; i < NUM_OF_BALLS; i++) {
            // in order to keep an even angle between all balls.
            double margin = i * ((double) (MAX_ANGLE - MIN_ANGLE) / (NUM_OF_BALLS - 1));
            Velocity v = Velocity.fromAngleAndSpeed(MIN_ANGLE + margin, BALL_SPEED);
            velocities.add(v);
        }
        return velocities;
    }

    @Override
    public int paddleSpeed() {
        return PADDLE_SPEED;
    }

    @Override
    public int paddleWidth() {
        return super.getSurfaceWidth() - SURFACE_PADDLE_DELTA;
    }

    @Override
    public String levelName() {
        return LEVEL_NAME;
    }

    @Override
    public Sprite getBackground() {
        Rectangle rectangle = new Rectangle(new Point(0, 0), super.getSurfaceWidth(), super.getSurfaceHeight());
        return new Background(BACKGROUND_COLOR, rectangle, null);
    }

    @Override
    public List<Block> blocks() {
        List<Block> blocks = new ArrayList<>();
        Color[] colors = {Color.GRAY, Color.YELLOW, Color.BLUE, Color.PINK, Color.GREEN, Color.RED};
        // we want the blocks to spread all over the screen, so we adjust their width to the given screen.
        double blockWidth = (super.getSurfaceWidth() - 2 * super.getBorderSize() - NUM_OF_BLOCKS * BLOCK_MARGIN)
                            / (double) NUM_OF_BLOCKS;
        double borderSize = super.getBorderSize();
        for (int i = 0; i < NUM_OF_BLOCKS; i++) {
            Point upperLeft = new Point(borderSize + i * (blockWidth + BLOCK_MARGIN), BLOCKS_Y);
            Rectangle rectangle = new Rectangle(upperLeft, blockWidth, BLOCK_HEIGHT);
            Block block = new Block(rectangle, colors[i % colors.length], BLOCK_MARGIN);
            blocks.add(block);
        }
        return blocks;
    }

    @Override
    public int numberOfBlocksToRemove() {
        return NUM_OF_BLOCKS;
    }
}
