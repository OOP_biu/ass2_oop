// 313285942
package levels;

import gamelogic.AnimationRunner;
import gamelogic.Counter;
import gamemanagement.CountdownAnimation;
import gamemanagement.KeyPressStoppableAnimation;
import gamemanagement.PauseScreen;
import gametools.Background;
import gametools.Block;
import gametools.Paddle;
import gametools.ScoreIndicator;
import gametools.Border;
import gametools.Ball;
import gametools.LevelNameIndicator;
import interfaces.LevelInformation;
import interfaces.Sprite;
import interfaces.Animation;
import interfaces.HitListener;
import interfaces.Collidable;
import listeners.BallRemover;
import listeners.BlockRemover;
import gamelogic.GameEnvironment;
import gamelogic.SpriteCollection;
import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import listeners.ScoreTrackingListener;

import java.awt.Color;
import java.util.List;

/**
 * The class GameLevel is responsible for creating all the sprites and collidables, and run the level.
 */
public class GameLevel implements Animation {
    private static final int TEXT_AREA_HEIGHT = 20;
    private static final int FONT_SIZE = 12;
    private static final Color TEXT_COLOR = Color.BLACK;
    private static final int TEXT_HEIGHT = TEXT_AREA_HEIGHT / 2;
    private static final int LEVEL_NAME_X = 200;
    private static final int FINISH_LEVEL_SCORE = 100;
    private static final double BLOCK_MARGIN = 1;
    private static final Color BORDER_COLOR = Color.GRAY;
    private static final double PADDLE_HEIGHT = 15;
    private static final Color PADDLE_COLOR = Color.ORANGE;
    private static final int BALL_RADIUS = 5;
    private static final Color BALL_COLOR = Color.WHITE;
    private static final int PADDLE_BALL_DISTANCE = 10;

    private LevelInformation levelInfo;
    private SpriteCollection sprites;
    private GameEnvironment environment;
    private Counter remainingBlocks;
    private Counter remainingBalls;
    private Counter score;
    private AnimationRunner runner;
    private boolean running;
    private KeyboardSensor sensor;
    private int surfaceWidth;
    private int surfaceHeight;
    private int borderSize;

    /**
     * Instantiates a new GameLevel.
     *
     * @param levelInfo     the information of the level.
     * @param ks            the ks
     * @param runner        the runner
     * @param score         the score
     * @param surfaceWidth  the surface width
     * @param surfaceHeight the surface height
     * @param borderSize    the border size
     */
    public GameLevel(LevelInformation levelInfo, KeyboardSensor ks, AnimationRunner runner, Counter score,
                     int surfaceWidth, int surfaceHeight, int borderSize) {
        this.levelInfo = levelInfo;
        this.sprites = new SpriteCollection();
        this.environment = new GameEnvironment();
        this.remainingBlocks = new Counter(this.levelInfo.numberOfBlocksToRemove());
        this.remainingBalls = new Counter(this.levelInfo.numOfBalls());
        this.score = score;
        this.runner = runner;
        this.running = false;
        this.surfaceWidth = surfaceWidth;
        this.surfaceHeight = surfaceHeight;
        this.sensor = ks;
        this.borderSize = borderSize;
    }

    /**
     * Add collidable to level environment.
     *
     * @param c the collidable.
     */
    public void addCollidable(Collidable c) {
        this.environment.addCollidable(c);
    }

    /**
     * Add sprite to sprites collection.
     *
     * @param s the sprite.
     */
    public void addSprite(Sprite s) {
        this.sprites.addSprite(s);
    }

    /**
     * Remove collidable from game environment.
     *
     * @param c the collidable.
     */
    public void removeCollidable(Collidable c) {
        this.environment.removeCollidable(c);
    }

    /**
     * Remove sprite from sprite collection.
     *
     * @param s the sprite.
     */
    public void removeSprite(Sprite s) {
        this.sprites.removeSprite(s);
    }

    /**
     * Initialize Game.
     */
    public void initialize() {
        addBackground();
        addBalls();
        addBorders();
        addBlocks();
        addPaddleAndDeathBlock();
        addTextArea();
        addScoreIndicator();
        addLevelNameIndicator();
    }

    /**
     * Run Game.
     */
    public void run() {
        this.running = true;
        // TODO magic numbers
        this.runner.run(new CountdownAnimation(2, 3, 1, this.sprites));
        this.runner.run(this);
    }

    /**
     * add white background to the area where text will bw shown.
     */
    private void addTextArea() {
        Rectangle rectangle = new Rectangle(new Point(0, 0), this.surfaceWidth, TEXT_AREA_HEIGHT);
        Background textArea = new Background(Color.WHITE, rectangle, null);
        textArea.addToGame(this);
    }

    /**
     * add level name indicator to game level.
     */
    private void addLevelNameIndicator() {
        String levelName = this.levelInfo.levelName();
        LevelNameIndicator indicator = new LevelNameIndicator("Level Name: " + levelName,
                                                this.surfaceWidth - LEVEL_NAME_X, TEXT_HEIGHT, FONT_SIZE, TEXT_COLOR);
        indicator.addToGame(this);
    }

    /**
     * add score indicator to game.
     */
    private void addScoreIndicator() {
        ScoreIndicator scoreIndicator = new ScoreIndicator(this.score, this.surfaceWidth / 2, TEXT_HEIGHT,
                                                            FONT_SIZE, TEXT_COLOR);
        scoreIndicator.addToGame(this);
    }

    /**
     * add blocks to game.
     */
    private void addBlocks() {
        HitListener blockRemoveListener = new BlockRemover(this, this.remainingBlocks);
        HitListener scoreListener = new ScoreTrackingListener(this.score);
        List<Block> blocks = this.levelInfo.blocks();
        for (Block block : blocks) {
            block.addToGame(this);
            block.addHitListener(blockRemoveListener);
            block.addHitListener(scoreListener);
        }
    }

    /**
     * add borders to game.
     */
    private void addBorders() {
        // rectangles
        Rectangle left = new Rectangle(new Point(0, this.borderSize + TEXT_AREA_HEIGHT),
                                        this.borderSize, this.surfaceHeight - 2 * this.borderSize + TEXT_AREA_HEIGHT);
        Rectangle right = new Rectangle(new Point(this.surfaceWidth - this.borderSize, this.borderSize  + TEXT_AREA_HEIGHT),
                                        this.borderSize, this.surfaceHeight - 2 * this.borderSize  + TEXT_AREA_HEIGHT);
        Rectangle up = new Rectangle(new Point(0, TEXT_AREA_HEIGHT), this.surfaceWidth, this.borderSize);
        // blocks
        Border leftBorder = new Border(left, BORDER_COLOR, (int) BLOCK_MARGIN);
        leftBorder.addToGame(this);
        Border rightBorder = new Border(right, BORDER_COLOR, (int) BLOCK_MARGIN);
        rightBorder.addToGame(this);
        Border upBorder = new Border(up, BORDER_COLOR, (int) BLOCK_MARGIN);
        upBorder.addToGame(this);
    }

    /**
     * add ball to game.
     */
    private void addBalls() {
        int numOfBalls = this.levelInfo.numOfBalls();
        if (numOfBalls == this.levelInfo.initialBallVelocities().size()) {
            for (Velocity v : this.levelInfo.initialBallVelocities()) {
                double x = this.surfaceWidth / 2.0 + v.getDx();
                double y = this.surfaceHeight - this.borderSize - PADDLE_HEIGHT - PADDLE_BALL_DISTANCE + v.getDy();
                Ball ball = new Ball(x, y, BALL_RADIUS, BALL_COLOR, v);
                ball.addToGame(this);
                ball.setGameEnvironment(this.environment);
            }
        }
    }

    /**
     * add Death Block to game.
     * @param paddle the paddle of the game.
     */
    private void addDeathBlock(Paddle paddle) {
        int deathBlockHeight = (int) (paddle.getPaddle().getHeight());
        Rectangle deathBlock = new Rectangle(new Point(0, this.surfaceHeight - deathBlockHeight),
                this.surfaceWidth, deathBlockHeight);
        Border downBorder = new Border(deathBlock, BORDER_COLOR, (int) BLOCK_MARGIN);
        downBorder.addToGame(this);
        // remove from sprite because we don't want it to be drawn
        this.removeSprite(downBorder);
        HitListener ballRemover = new BallRemover(this, this.remainingBalls);
        downBorder.addHitListener(ballRemover);
    }

    /**
     * add paddle to game.
     */
    private void addPaddleAndDeathBlock() {
        int paddleWidth = this.levelInfo.paddleWidth();
        Point upperLeft = new Point(((double) this.surfaceWidth / 2) - ((double) paddleWidth / 2),
                                    this.surfaceHeight - PADDLE_HEIGHT);
        Rectangle rectangle = new Rectangle(upperLeft, paddleWidth, PADDLE_HEIGHT);
        Paddle paddle = new Paddle(this.sensor, rectangle, this.surfaceWidth, this.borderSize,
                                    PADDLE_COLOR, this.levelInfo.paddleSpeed());
        paddle.addToGame(this);
        addDeathBlock(paddle);
    }

    /**
     * Add the background of the screen.
     */
    private void addBackground() {
        this.addSprite(this.levelInfo.getBackground());
    }

    /**
     * checks if level has blocks left.
     *
     * @return true if yes, false if not.
     */
    public boolean hasBlocksLeft() {
        return !(this.remainingBlocks.getValue() == 0);
    }

    /**
     * checks if level has balls left.
     *
     * @return true if yes, false if not.
     */
    public boolean hasBallsLeft() {
        return !(this.remainingBalls.getValue() == 0);
    }

    // Animation interface methods

    @Override
    public void doOneFrame(DrawSurface d) {
        this.sprites.drawAllOn(d);
        this.sprites.notifyAllTimePassed();

        // pause screen
        if (this.sensor.isPressed("p")) {
            KeyPressStoppableAnimation animation = new KeyPressStoppableAnimation(this.sensor, KeyboardSensor.SPACE_KEY,
                                                                                    new PauseScreen());
            this.runner.run(animation);
        }

        if (this.remainingBalls.getValue() == 0) {
            this.running = false;
        }
        if (this.remainingBlocks.getValue() == 0) {
            this.score.increase(FINISH_LEVEL_SCORE);
            this.running = false;
        }
    }

    @Override
    public boolean shouldStop() {
        return !this.running;
    }
}
