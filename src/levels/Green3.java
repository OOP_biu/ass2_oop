package levels;

import gametools.Background;
import gametools.Block;
import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import interfaces.Sprite;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Green 3.
 */
public class Green3 extends BasicLevel {
    private static final int NUM_OF_BALLS = 2;
    private static final int LEFT_BALL_ANGLE = -50;
    private static final int RIGHT_BALL_ANGLE = 50;
    private static final int BALL_SPEED = 6;
    private static final int PADDLE_SPEED = 10;
    private static final int PADDLE_WIDTH = 100;
    private static final String LEVEL_NAME = "Green 3";
    private static final Color BACKGROUND_COLOR = new Color(0, 102, 0);
    private static final int NUM_OF_ROWS = 5;
    private static final int MAX_BLOCKS_IN_ROW = 10;
    private static final double BLOCK_WIDTH = 50;
    private static final double BLOCK_MARGIN = 1;
    private static final double BLOCKS_START_Y = 130;
    private static final double BLOCK_HEIGHT = 25;

    /**
     * Instantiates a new Green 3 level.
     *
     * @param surfaceWidth  the surface width
     * @param surfaceHeight the surface height
     * @param borderSize    the border size
     */
    public Green3(int surfaceWidth, int surfaceHeight, int borderSize) {
        super(surfaceWidth, surfaceHeight, borderSize);
    }

    @Override
    public int numOfBalls() {
        return NUM_OF_BALLS;
    }

    @Override
    public List<Velocity> initialBallVelocities() {
        List<Velocity> velocities = new ArrayList<>();
        Velocity v1 = Velocity.fromAngleAndSpeed(LEFT_BALL_ANGLE, BALL_SPEED);
        Velocity v2 = Velocity.fromAngleAndSpeed(RIGHT_BALL_ANGLE, BALL_SPEED);
        velocities.add(v1);
        velocities.add(v2);
        return velocities;
    }

    @Override
    public int paddleSpeed() {
        return PADDLE_SPEED;
    }

    @Override
    public int paddleWidth() {
        return PADDLE_WIDTH;
    }

    @Override
    public String levelName() {
        return LEVEL_NAME;
    }

    @Override
    public Sprite getBackground() {
        Rectangle rectangle = new Rectangle(new Point(0, 0), super.getSurfaceWidth(), super.getSurfaceHeight());
        return new Background(BACKGROUND_COLOR, rectangle, null);
    }

    @Override
    public List<Block> blocks() {
        List<Block> blocks = new ArrayList<>();
        Color[] colors = {Color.GRAY, Color.RED, Color.YELLOW, Color.BLUE, Color.WHITE};
        double surfaceWidth = super.getSurfaceWidth();
        double borderSize = super.getBorderSize();
        for (int i = 0; i < NUM_OF_ROWS; i++) {
            for (int j = 0; j + i < MAX_BLOCKS_IN_ROW; j++) {
                double startX = surfaceWidth - borderSize - (MAX_BLOCKS_IN_ROW * (BLOCK_WIDTH + BLOCK_MARGIN));
                Point upperLeft = new Point(startX + (BLOCK_MARGIN + BLOCK_WIDTH) * (j + i),
                        BLOCKS_START_Y + (BLOCK_MARGIN + BLOCK_HEIGHT) * i);
                Rectangle rectangle = new Rectangle(upperLeft, BLOCK_WIDTH, BLOCK_HEIGHT);
                // the modulo is because we have a fixed number of colors, but the num of rows in not fixed.
                Block block = new Block(rectangle, colors[i % NUM_OF_ROWS], (int) BLOCK_MARGIN);
                blocks.add(block);
            }
        }
        return blocks;
    }

    @Override
    public int numberOfBlocksToRemove() {
        int numOfBlocks = 0;
        for (int i = 0; i < NUM_OF_ROWS; i++) {
            numOfBlocks += MAX_BLOCKS_IN_ROW - i;
        }
        return numOfBlocks;
    }
}
