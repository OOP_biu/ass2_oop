package levels;

import gametools.Background;
import gametools.Block;
import geometry.Point;
import geometry.Rectangle;
import geometry.Velocity;
import interfaces.Sprite;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Direct hit level.
 */
public class DirectHit extends BasicLevel {
    private static final double BALL_ANGLE = 0;
    private static final double BALL_SPEED = 6;
    private static final int PADDLE_SPEED = 10;
    private static final int PADDLE_WIDTH = 100;
    private static final double BLOCK_Y = 60;
    private static final double BLOCK_HEIGHT = 30;
    private static final double BLOCK_WIDTH = 70;
    private static final Color BLOCK_COLOR = Color.RED;
    private static final String LEVEL_NAME = "Direct Hit";
    private static final int NUM_OF_BALLS = 1;
    private static final int NUM_OF_BLOCKS = 1;
    private static final Color BACKGROUND_COLOR = Color.BLACK;

    /**
     * Instantiates a new Direct Hit level.
     *
     * @param surfaceWidth  the surface width
     * @param surfaceHeight the surface height
     * @param borderSize    the border size
     */
    public DirectHit(int surfaceWidth, int surfaceHeight, int borderSize) {
        super(surfaceWidth, surfaceHeight, borderSize);
    }

    @Override
    public int numOfBalls() {
        return NUM_OF_BALLS;
    }

    @Override
    public List<Velocity> initialBallVelocities() {
        List<Velocity> velocities = new ArrayList<>();
        Velocity velocity = Velocity.fromAngleAndSpeed(BALL_ANGLE, BALL_SPEED);
        velocities.add(velocity);
        return velocities;
    }

    @Override
    public int paddleSpeed() {
        return PADDLE_SPEED;
    }

    @Override
    public int paddleWidth() {
        return PADDLE_WIDTH;
    }

    @Override
    public String levelName() {
        return LEVEL_NAME;
    }

    @Override
    public Sprite getBackground() {
        Rectangle rectangle = new Rectangle(new Point(0, 0), super.getSurfaceWidth(), super.getSurfaceHeight());
        // TODO add image
        return new Background(BACKGROUND_COLOR, rectangle, null);
    }

    @Override
    public List<Block> blocks() {
        List<Block> blocks = new ArrayList<>();
        Point upperLeft = new Point((super.getSurfaceWidth() / 2.0) - (BLOCK_WIDTH / 2.0), BLOCK_Y);
        Rectangle rectangle = new Rectangle(upperLeft, BLOCK_WIDTH, BLOCK_HEIGHT);
        Block block = new Block(rectangle, BLOCK_COLOR, 0);
        blocks.add(block);
        return blocks;
    }

    @Override
    public int numberOfBlocksToRemove() {
        return NUM_OF_BLOCKS;
    }
}
