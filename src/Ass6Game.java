import biuoop.GUI;
import biuoop.KeyboardSensor;
import gamelogic.AnimationRunner;
import gamemanagement.GameFlow;
import interfaces.LevelInformation;
import levels.*;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Ass 6 game.
 */
public class Ass6Game {
    private static final int SURFACE_WIDTH = 800;
    private static final int SURFACE_HEIGHT = 600;
    private static final int BORDER_SIZE = 20;
    private static final int FRAMES_PER_SECOND = 60;
    private static final int DIRECT_HIT = 1;
    private static final int WIDE_EASY = 2;
    private static final int GREEN_3 = 3;
    private static final int FINAL_FOUR = 4;

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        runGame(args);
    }

    private static void runGame(String[] args) {
        GUI gui = new GUI("Arkanoid", SURFACE_WIDTH, SURFACE_HEIGHT);
        AnimationRunner runner = new AnimationRunner(gui, FRAMES_PER_SECOND);
        KeyboardSensor sensor = gui.getKeyboardSensor();
        int width = gui.getDrawSurface().getWidth();
        int height = gui.getDrawSurface().getHeight();
        GameFlow game = new GameFlow(runner, sensor, width, height, BORDER_SIZE);
        List<LevelInformation> levels = createLevels(args, width, height);
        game.runLevels(levels);
        gui.close();
    }

    /**
     * create list of the levels to be played according to arguments received.
     * @param args the arguments received from user
     * @param width of the screen
     * @param height of the screen
     * @return the list.
     */
    private static List<LevelInformation> createLevels(String[] args, int width, int height) {
        List<LevelInformation> levels = new ArrayList<>();
        // default levels
        if (args.length == 0) {
            DirectHit directHit = new DirectHit(width, height, BORDER_SIZE);
            levels.add(directHit);
            WideEasy wideEasy = new WideEasy(width, height, BORDER_SIZE);
            levels.add(wideEasy);
            Green3 green3 = new Green3(width, height, BORDER_SIZE);
            levels.add(green3);
            FinalFour finalFour = new FinalFour(width, height, BORDER_SIZE);
            levels.add(finalFour);
        }
        // according to arguments
        for (String s : args) {
            try {
                int level = Integer.parseInt(s);
                switch (level) {
                    case DIRECT_HIT:
                        DirectHit directHit = new DirectHit(width, height, BORDER_SIZE);
                        levels.add(directHit);
                        break;
                    case WIDE_EASY:
                        WideEasy wideEasy = new WideEasy(width, height, BORDER_SIZE);
                        levels.add(wideEasy);
                        break;
                    case GREEN_3:
                        Green3 green3 = new Green3(width, height, BORDER_SIZE);
                        levels.add(green3);
                        break;
                    case FINAL_FOUR:
                        FinalFour finalFour = new FinalFour(width, height, BORDER_SIZE);
                        levels.add(finalFour);
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                System.out.print("");
            }
        }
        return levels;
    }
}
