// 313285942

import biuoop.GUI;
import biuoop.KeyboardSensor;
import gamelogic.AnimationRunner;
import gamelogic.Counter;
import levels.*;


/**
 * The type Ass 5 game.
 */
public class Ass5Game {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
       // DirectHit directHit = new DirectHit(800, 600);
        //WideEasy wideEasy = new WideEasy(800, 600, 20);
        //Green3 green3 = new Green3(800, 600, 20);
        FinalFour finalFour = new FinalFour(800, 600, 20);
        GUI gui = new GUI("Arkanoid", 800, 600);
        AnimationRunner runner = new AnimationRunner(gui, 60);
        KeyboardSensor sensor = gui.getKeyboardSensor();
        Counter score = new Counter();
        GameLevel gameLevel = new GameLevel(finalFour, sensor, runner, score, 800, 600, 20);
        gameLevel.initialize();
        gameLevel.run();
        gui.close();
    }
}
