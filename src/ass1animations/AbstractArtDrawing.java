// 313285942
package ass1animations;

import geometry.Line;
import geometry.Point;
import biuoop.GUI;
import biuoop.DrawSurface;
import java.util.Random;
import java.awt.Color;

/**
 * The Class creates random lines and shows them, also marking their middle point and intersections between them.
 * The class uses the geometry.Line and geometry.Point classes.
 */
public class AbstractArtDrawing {

    /**
     * Draw random lines, also marking their middle point and intersections between them.
     */
    public void drawRandomLines() {
        GUI gui = new GUI("Random Lines", 400, 300);
        DrawSurface d = gui.getDrawSurface();
        Line[] randomLines =  new Line[10];
        // draws the lines and their middle point
        for (int i = 0; i < 10; i++) {
            randomLines[i] = generateRandomLine();
            d.setColor(Color.BLACK);
            this.drawLine(randomLines[i], d);
            d.setColor(Color.BLUE);
            d.fillCircle((int) randomLines[i].middle().getX(), (int) randomLines[i].middle().getY(), 3);
        }
        // draws the intersection points between the lines
        d.setColor(Color.RED);
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Point intersection = randomLines[i].intersectionWith(randomLines[j]);
                if (intersection != null) {
                    d.fillCircle((int) intersection.getX(), (int) intersection.getY(), 3);
                }
            }
        }
        gui.show(d);
    }

    /**
     * Draw line on surface.
     *
     * @param randomLine the line to be drawn
     * @param d          the surface
     */
    public void drawLine(Line randomLine, DrawSurface d) {
        d.drawLine((int) randomLine.start().getX(), (int) randomLine.start().getY(),
                   (int) randomLine.end().getX(), (int) randomLine.end().getY());
    }

    /**
     * Generate a random line.
     *
     * @return the line
     */
    public Line generateRandomLine() {
        Random rand = new Random();
        int x1 = rand.nextInt(400) + 1;
        int y1 = rand.nextInt(300) + 1;
        int x2 = rand.nextInt(400) + 1;
        int y2 = rand.nextInt(300) + 1;
        Point first = new Point((double) x1, (double) y1);
        Point second = new Point((double) x2, (double) y2);
        return new Line(first, second);
    }

    /**
     * The entry point of application.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AbstractArtDrawing example = new AbstractArtDrawing();
        example.drawRandomLines();
    }
}

