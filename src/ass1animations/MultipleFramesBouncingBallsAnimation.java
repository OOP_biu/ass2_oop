// 313285942
package ass1animations;

import gametools.Ball;
import biuoop.GUI;
import biuoop.DrawSurface;
import biuoop.Sleeper;
import java.util.Arrays;
import java.awt.Color;

/**
 * The class creates a multiple bouncing balls in frames animation.
 */
public class MultipleFramesBouncingBallsAnimation {
    static final int MAX_SURFACE_WIDTH = 800;
    static final int MAX_SURFACE_HEIGHT = 800;
    static final int GREY_MIN_X = 50;
    static final int GREY_MIN_Y = 50;
    static final int GREY_SIZE = 450;
    static final int GREY_MAX_X = 500;
    static final int GREY_MAX_Y = 500;
    static final int YELLOW_MIN_X = 450;
    static final int YELLOW_MIN_Y = 450;
    static final int YELLOW_SIZE = 150;
    static final int YELLOW_MAX_X = 600;
    static final int YELLOW_MAX_Y = 600;
    static final int SLEEP_TIME = 50;


    /**
     * Draw a multiple bouncing balls in frames animation from given radius array.
     *
     * @param radiusInString the radius array represented by string
     */
    private static void drawAnimation(String[] radiusInString) {
        // separate grey from yellow balls
        int middle = radiusInString.length / 2;
        String[] greyRadius = Arrays.copyOfRange(radiusInString, 0, middle);
        String[] yellowRadius = Arrays.copyOfRange(radiusInString, middle, radiusInString.length);

        // create balls
        Ball[] greyBalls = MultipleBouncingBallsAnimation.createBalls(greyRadius,
                GREY_MIN_X, GREY_MIN_Y, GREY_MAX_X, GREY_MAX_Y);
        Ball[] yellowBalls = MultipleBouncingBallsAnimation.createBalls(yellowRadius,
                YELLOW_MIN_X, YELLOW_MIN_Y, YELLOW_MAX_X, YELLOW_MAX_Y);
        drawBallsInFrames(greyBalls, yellowBalls);
    }

    /**
     * Draw frames and then the given balls inside the frames.
     *
     * @param greyBalls   the grey balls array
     * @param yellowBalls the yellow balls array
     */
    private static void drawBallsInFrames(Ball[] greyBalls, Ball[] yellowBalls) {
        GUI gui = new GUI("Multiple Frames Bouncing Balls Animation", MAX_SURFACE_WIDTH, MAX_SURFACE_HEIGHT);
        Sleeper sleeper = new Sleeper();
        while (true) {
            DrawSurface d = gui.getDrawSurface();
            drawFrames(d);
            drawBalls(greyBalls, d, GREY_MIN_X, GREY_MIN_Y, GREY_MAX_X, GREY_MAX_Y);
            drawBalls(yellowBalls, d, YELLOW_MIN_X, YELLOW_MIN_Y, YELLOW_MAX_X, YELLOW_MAX_Y);
            sleeper.sleepFor(SLEEP_TIME);
            gui.show(d);
        }
    }

    /**
     * Draw given balls inside boundaries on a given surface.
     *
     * @param balls   the balls array
     * @param d the surface to draw on
     * @param minX the minimum X coordinate
     * @param minY the minimum Y coordinate
     * @param maxX the maximum X coordinate
     * @param maxY the maximum X coordinate
     */
    private static void drawBalls(Ball[] balls, DrawSurface d, int minX, int minY, int maxX, int maxY) {
        for (Ball ball : balls) {
            ball.moveOneStep(minX, minY, maxX, maxY);
            ball.drawOn(d);
        }
    }

    /**
     * Draw frames on a given surface.
     *
     * @param d the surface to draw on
     */
    private static void drawFrames(DrawSurface d) {
        d.setColor(Color.GRAY);
        d.fillRectangle(GREY_MIN_X, GREY_MIN_X, GREY_SIZE, GREY_SIZE);
        d.setColor(Color.YELLOW);
        d.fillRectangle(YELLOW_MIN_X, YELLOW_MIN_Y, YELLOW_SIZE, YELLOW_SIZE);
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws RuntimeException when no arguments were passed
     */
    public static void main(String[] args) throws RuntimeException {
        if (args.length > 0) {
            drawAnimation(args);
        } else {
            throw new RuntimeException("no arguments passed");
        }
    }
}
