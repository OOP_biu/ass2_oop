// 313285942
package ass1animations;

import geometry.Velocity;
import geometry.Point;
import gametools.Ball;
import biuoop.DrawSurface;
import biuoop.GUI;

/**
 * The class draws a bouncing ball animation.
 */
public class BouncingBallAnimation {
    static final int WIDTH = 400;
    static final int HEIGHT = 400;
    static final int SLEEP_TIME = 50;
    static final int RADIUS = 30;

    /**
     * Draws animation of a bouncing ball.
     * @param start the start point of the ball
     * @param dx the velocity's dx
     * @param dy the velocity's dy
     */
    private static void drawAnimation(Point start, double dx, double dy) {
        GUI gui = new GUI("title", WIDTH, HEIGHT);
        biuoop.Sleeper sleeper = new biuoop.Sleeper();
        Ball ball = new Ball(start.getX(), start.getY(), RADIUS, java.awt.Color.BLACK);
        ball.setVelocity(dx, dy);
        while (true) {
            ball.moveOneStep(WIDTH, HEIGHT);
            DrawSurface d = gui.getDrawSurface();
            ball.drawOn(d);
            gui.show(d);
            sleeper.sleepFor(SLEEP_TIME);  // wait for 50 milliseconds.
        }
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws RuntimeException when not enough arguments were passed
     */
    public static void main(String[] args) throws RuntimeException {
        if (args.length == 4) {
            double x = Double.parseDouble(args[0]);
            double y = Double.parseDouble(args[1]);
            Point center = new Point(x, y);
            if (x - RADIUS < 0 || x + RADIUS > WIDTH || y - RADIUS < 0 || y + RADIUS > HEIGHT) {
                throw new RuntimeException("ball must appear fully in surface");
            }
            double angle = Double.parseDouble(args[2]);
            double speed = Double.parseDouble(args[3]);
            Velocity velocity = Velocity.fromAngleAndSpeed(angle, speed);
            drawAnimation(center, velocity.getDx(), velocity.getDy());
        } else {
            throw new RuntimeException("not enough arguments");
        }
    }
}
