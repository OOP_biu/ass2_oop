// 313285942
package ass1animations;

import gametools.Ball;
import geometry.Velocity;
import geometry.Point;
import biuoop.GUI;
import biuoop.DrawSurface;
import biuoop.Sleeper;

import java.util.Random;
import java.awt.Color;

/**
 * The class creates a multiple bouncing balls animation.
 */
public class MultipleBouncingBallsAnimation {
    static final int MIN_X = 0;
    static final int MIN_Y = 0;
    static final int MAX_X = 800;
    static final int MAX_Y = 500;
    static final int MAX_ANGLE = 360;
    static final double MIN_SPEED = 1;
    static final int SLEEP_TIME = 50;

    /**
     * Draw animation.
     *
     * @param balls  the balls
     * @param width  the width of the surface
     * @param height the height of the surface
     */
    private static void drawAnimation(Ball[] balls, int width, int height) {
        GUI gui = new GUI("Multiple Bouncing Balls Animation", width, height);
        Sleeper sleeper = new Sleeper();
        while (true) {
            DrawSurface d = gui.getDrawSurface();
            for (Ball ball: balls) {
                ball.moveOneStep(width, height);
                ball.drawOn(d);
            }
            sleeper.sleepFor(SLEEP_TIME);
            gui.show(d);
        }
    }

    /**
     * Create balls array with random locations, colors and velocities, from a given array of radius.
     * The minimum and maximum X and Y coordinates represent the boundaries of the balls.
     *
     * @param radiusInString the radius represented by strings
     * @param minX           the min x
     * @param minY           the min y
     * @param maxX           the max x
     * @param maxY           the max y
     * @return the balls array.
     */
    public static Ball[] createBalls(String[] radiusInString, int minX, int minY, int maxX, int maxY) {
        int length = radiusInString.length;
        int[] radiusArr = generateRadius(radiusInString);
        Point[] centers = generateRandomCenterPoints(radiusArr, minX, minY, maxX, maxY);
        Color[] colors = generateRandomColor(length);
        Velocity[] velocities = generateVelocities(length, radiusArr);
        // generate the balls from the arrays above
        Ball[] balls = new Ball[length];
        for (int i = 0; i < length; i++) {
            balls[i] = new Ball(centers[i], radiusArr[i], colors[i], velocities[i]);
        }
        return balls;
    }

    /**
     * Generate radius array represented by int.
     *
     * @param radiusInString the radius represented by string.
     * @return the ints array.
     */
    private static int[] generateRadius(String[] radiusInString) {
        int[] radiusArr = new int[radiusInString.length];
        for (int i = 0; i < radiusInString.length; i++) {
            radiusArr[i] = Integer.parseInt(radiusInString[i]);
        }
        return radiusArr;
    }

    /**
     * Generate velocities array.
     * The speed is proportional to the ball's size, but the angle is random.
     *
     * @param length    the length of the radius array - how many velocities needed.
     * @param radiusArr the radius array
     * @return the velocities array.
     */
    private static Velocity[] generateVelocities(int length, int[] radiusArr) {
        Random rand = new Random();
        Velocity[] velocities = new Velocity[length];
        for (int i = 0; i < length; i++) {
            double angle = (double) rand.nextInt(MAX_ANGLE);
            double speed;
            if (radiusArr[i] >= 50) {
                speed = MIN_SPEED;
            }
            speed = (double) 50 / radiusArr[i];
            velocities[i] = Velocity.fromAngleAndSpeed(angle, speed);
        }
        return velocities;
    }

    /**
     * Generate random color array.
     *
     * @param length - how many colors needed.
     * @return the color array.
     */
    private static Color[] generateRandomColor(int length) {
        Random rand = new Random();
        Color[] colors = new Color[length];
        for (int i = 0; i < length; i++) {
            float r = rand.nextFloat();
            float g = rand.nextFloat();
            float b = rand.nextFloat();
            colors[i] = new Color(r, g, b);
        }
        return colors;
    }

    /**
     * Generate random center points array.
     * The minimum and maximum X and Y coordinates represent the boundaries of the balls.
     *
     * @param radiusArr the radius array
     * @param minX      the min x
     * @param minY      the min y
     * @param maxX      the max x
     * @param maxY      the max y
     * @return the centers array
     * @throws RuntimeException when ball is bigger than the frame
     */
    private static Point[] generateRandomCenterPoints(int[] radiusArr, int minX, int minY, int maxX, int maxY)
            throws RuntimeException {
        Random rand = new Random();
        Point[] centers = new Point[radiusArr.length];
        for (int i = 0; i < radiusArr.length; i++) {
            if (2 * radiusArr[i] >= maxX - minX || 2 * radiusArr[i] >= maxY - minY) {
                throw new RuntimeException("ball is bigger than frame");
            }
            int x = rand.nextInt(maxX - minX) + minX;
            // we don't want the ball to appear not fully in screen
            while (x - radiusArr[i] <= minX || x + radiusArr[i] >= maxX) {
                x = rand.nextInt(maxX - minX) + minX;
            }
            int y = rand.nextInt(maxY - minY) + minY;
            // we don't want the ball to appear not fully in screen
            while (y - radiusArr[i] <= minY || y + radiusArr[i] >= maxY) {
                y = rand.nextInt(maxY - minY) + minY;
            }
            centers[i] = new Point((double) x, (double) y);
        }
        return centers;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws RuntimeException when no arguments were passed
     */
    public static void main(String[] args) throws RuntimeException {
        if (args.length > 0) {
            Ball[] balls = createBalls(args, MIN_X, MIN_Y, MAX_X, MAX_Y);
            drawAnimation(balls, MAX_X, MAX_Y);
        } else {
            throw new RuntimeException("no arguments passed");
        }
    }
}
