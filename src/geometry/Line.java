// 313285942
package geometry;
import java.util.List;

/**
 * The type geometry.Line.
 * The class supports the methods - length, middle, getStart, getEnd,isIntersecting, intersectionWith, equals,
 *                                  isVertical, isOverlap, findSlope, findFreeElement and isPointInLine.
 * It is implemented by two points, which used as edges of the line segment.
 */
public class Line {
    private Point start;
    private Point end;

    // constructors
    /**
     * Instantiates a new geometry.Line with two given points.
     *
     * @param start the start point.
     * @param end   the end point.
     */
    public Line(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    /**
     * Instantiates a new geometry.Line with four coordinates.
     *
     * @param x1 the x coordinate of the start point.
     * @param y1 the y coordinate of the start point.
     * @param x2 the x coordinate of the end point.
     * @param y2 the y coordinate of the end point.
     */
    public Line(double x1, double y1, double x2, double y2) {
        Point first = new Point(x1, y1);
        Point second = new Point(x2, y2);
        this.start = first;
        this.end = second;
    }
    // queries
    /**
     * Calculates the length of the line segment.
     *
     * @return the length.
     */
    public double length() {
        return this.start.distance(this.end);
    }

    /**
     * Find the middle point of the line segment.
     *
     * @return the middle point.
     */
    public Point middle() {
        double midX = (this.end.getX() + this.start.getX()) / 2;
        double midY = (this.end.getY() + this.start.getY()) / 2;
        return new Point(midX, midY);
    }

    /**
     * Get start point.
     *
     * @return the start point.
     */
    public Point start() {
        return this.start;
    }

    /**
     * Get end point.
     *
     * @return the end point.
     */
    public Point end() {
        return this.end;
    }


    /**
     * Checks if the line has an intersection point with a given line.
     *
     * @param other the other line.
     * @return True if intersecting, False if not.
     */
    public boolean isIntersecting(Line other) {
        // the function "intersectionWith" returns null if there is no intersection
        return this.intersectionWith(other) != null;
    }


    /**
     * Finds the intersection point between the line and a given line.
     *
     * @param other the other line.
     * @return the intersection point. Null if there is no intersection.
     */
    public Point intersectionWith(Line other) {
        // same lines or overlapping lines have infinity intersection points, so they are not defined as intersecting
        if (this.equals(other) || this.isOverlap(other)) {
            return null;
        }

        // if one of the edges of the line segments are equal
        if (this.start.equals(other.start) || this.start.equals(other.end)) {
            return this.start;
        }
        if (this.end.equals(other.end) || this.end.equals(other.start)) {
            return this.end;
        }

        /*
        if both of the lines are vertical, then they are parallel and don't intersect.
        they can still have the same start/end, but that was checked in earlier conditions.
         */
        if (this.isVertical() && other.isVertical()) {
            return null;
        }

        // the coordinates of the intersection point
        double intersectX;
        double intersectY;

        // when one of the lines is vertical
        if (this.isVertical()) {
            intersectX = this.start.getX();
            double otherSlope = other.findSlope();
            double otherFreeElement = other.findFreeElement(other.start.getX(), other.start.getY(), otherSlope);
            // y = m * x + b
            intersectY = otherSlope * intersectX + otherFreeElement;
            Point intersection = new Point(intersectX, intersectY);
            if (this.isPointInLine(intersection) && other.isPointInLine(intersection)) {
                return intersection;
            }
            return null;
        }
        if (other.isVertical()) {
            intersectX = other.start.getX();
            double mySlope = this.findSlope();
            double myFreeElement = this.findFreeElement(this.start.getX(), this.start.getY(), mySlope);
            // y = m * x + b
            intersectY = mySlope * intersectX + myFreeElement;
            Point intersection = new Point(intersectX, intersectY);
            if (this.isPointInLine(intersection) && other.isPointInLine(intersection)) {
                return intersection;
            }
            return null;
        }
        double mySlope = this.findSlope();
        double otherSlope = other.findSlope();
        if (mySlope == otherSlope) {
            return null;
        }
        double myFreeElement = this.findFreeElement(this.start.getX(), this.start.getY(), mySlope);
        double otherFreeElement = other.findFreeElement(other.start.getX(), other.start.getY(), otherSlope);
        // according to: m1 * x + b1 = m2 * x + b2 =====> x = (b2 - b1)/(m1 - m2)
        intersectX = (otherFreeElement - myFreeElement) / (mySlope - otherSlope);
        // y = m * x + b
        intersectY = mySlope * intersectX + myFreeElement;
        Point intersectionPoint = new Point(intersectX, intersectY);
        /*
        we found the intersection of to infinite lines.
        because we are dealing with line segments, we have to check the the point are in the segment.
        */
        if (this.isPointInLine(intersectionPoint) && other.isPointInLine(intersectionPoint)) {
            return intersectionPoint;
        }
        return null;
    }

    /**
     * Checks if the line is equal to a given line.
     *
     * @param other the other line.
     * @return True if they are equal, False if not.
     */
    public boolean equals(Line other) {
        return this.start.equals(other.start) && this.end.equals(other.end);
    }

    /**
     * Checks if lines overlap.
     *
     * @param other the other line
     * @return True if they overlap, False if not.
     */
    public boolean isOverlap(Line other) {
        // if the lines are opposite then they overlap
        if (this.start.equals(other.end) && this.end.equals(other.start)) {
            return true;
        }
        // if both of them are vertical then we need to check for overlap
        if (this.isVertical() && other.isVertical()) {
            if (this.start.getX() != other.start.getX()) {
                return false;
            }
            return this.isPointInLine(other.start) || this.isPointInLine(other.end);
        }
        // if only one of them is vertical then they don't overlap
        if (this.isVertical() || other.isVertical()) {
            return false;
        }
        double mySlope = this.findSlope();
        double otherSlope = other.findSlope();
        // if lines don't have the same slope they don't overlap
        if (mySlope != otherSlope) {
            return false;
        }
        // if the lines have the same slope, and a point of one line is inside the other line, they overlap
        return ((this.isPointInLine(other.start) && !this.start.equals(other.start) && !this.end.equals(other.start))
                || (this.isPointInLine(other.end) && !this.start.equals(other.end) && !this.end.equals(other.end)));
    }

    /**
     * Checks if the line is vertical.
     *
     * @return True if vertical, False if not.
     */
    public boolean isVertical() {
        return this.end.getX() == this.start.getX();
    }


    /**
     * Finds the closest intersection with a given rectangle point to the start of line.
     *
     * @param rect the rectangle
     * @return the closest point
     */
    public Point closestIntersectionToStartOfLine(Rectangle rect) {
        List<Point> intersections = rect.intersectionPoints(this);
        // when there are no intersections with rectangle
        if (intersections.isEmpty()) {
            return null;
        }
        Point closest = intersections.get(0);
        for (Point intersection : intersections) {
            if (intersection.distance(this.start) < closest.distance(this.start)) {
                closest = intersection;
            }
        }
        return closest;
    }


    /**
     * Checks if point is in line.
     *
     * @param p1 the point to be checked.
     * @return True if point in line, False if not.
     */
    public boolean isPointInLine(Point p1) {
        double epsilon = Math.pow(10, -10);
        return Math.abs(p1.distance(this.start) + p1.distance(this.end) - this.length()) <= epsilon;
    }

    /**
     * Find the slope of the line.
     *
     * @return the slope.
     * @throws RuntimeException when line is vertical.
     */
    private double findSlope() throws RuntimeException {
        if (this.isVertical()) {
            throw new RuntimeException("line is vertical - slope is undefined");
        }
        // the slope is deltaY / deltaX
        if (this.end.getX() > this.start.getX()) {
            return (this.end.getY() - this.start.getY()) / (this.end.getX() - this.start.getX());
        } else {
            return (this.start.getY() - this.end.getY()) / (this.start.getX() - this.end.getX());
        }
    }

    /**
     * Find the free element of the line equation.
     *
     * @param x     the x coordinate.
     * @param y     the y coordinate.
     * @param slope the slope of the line.
     * @return the free element.
     */
    private double findFreeElement(double x, double y, double slope) {
        // according to: y = m * x + b ====> b = y - m * x
        return y - (slope * x);
    }

    /**
     * Creating a String representation for the line.
     *
     * @return the string.
     */
    public String toString() {
        return "geometry.Line{" + "start=" + this.start + ", end=" + this.end + '}';
    }
}
