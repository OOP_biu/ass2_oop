// 313285942
package geometry;
import java.util.ArrayList;
import java.util.List;

/**
 * The type geometry.Rectangle.
 * The class supports the methods: Getters, intersectionPoints.
 * Te class uses the geometry.Point and geometry.Line classes.
 * Implemented by upper-left corner, width and height of rectangle.
 */
public class Rectangle {
    private static final int NUM_OF_EDGES = 4;
    public static final int LEFT_EDGE = 0;
    public static final int UP_EDGE = 1;
    public static final int RIGHT_EDGE = 2;
    public static final int DOWN_EDGE = 3;
    public static final int UP_LEFT = 0;
    public static final int UP_RIGHT = 1;
    public static final int DOWN_RIGHT = 2;
    public static final int DOWN_LEFT = 3;

    private Point upperLeft;
    private double width;
    private double height;

    /**
     * Instantiates a new geometry.Rectangle.
     *
     * @param upperLeft the upper left corner
     * @param width     the width
     * @param height    the height
     */
    public Rectangle(Point upperLeft, double width, double height) {
        this.upperLeft = new Point(upperLeft.getX(), upperLeft.getY());
        this.width = width;
        this.height = height;
    }

    /**
     * Gets upper left corner.
     *
     * @return the upper left corner
     */
    public Point getUpperLeft() {
        return this.upperLeft;
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    public double getWidth() {
        return this.width;
    }

    /**
     * Gets height.
     *
     * @return the height
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * Sets upper left point.
     *
     * @param point the upper left point.
     */
    public void setUpperLeft(Point point) {
        this.upperLeft =  new Point(point.getX(), point.getY());
    }

    /**
     * The function creates a list of all the intersection points the rectangle edges' have with a given line.
     *
     * @param line the given line
     * @return the list
     */
    public List<Point> intersectionPoints(Line line) {
        // TODO to use linked list or another kind of list?
        List<Point> intersections = new ArrayList<Point>();
        Line[] edges = this.createEdges();
        boolean isOverlap = false;
        for (Line edge : edges) {
            Point intersection = edge.intersectionWith(line);
            if (intersection != null && !intersection.isPointInList(intersections)) {
                intersections.add(intersection);
            } else if (edge.isOverlap(line)) {
                isOverlap = true;
            }
        }
        /*
         because every corner belongs to two edges, if there is an overlap,
         there will be intersection with the other edge. So we must clear the list from this point.
         */
        if (isOverlap) {
            intersections.clear();
        }
        return intersections;
    }

    /**
     * The function creates an array that holds the four edges of the rectangle.
     * @return the array
     */
    public Line[] createEdges() {
        Line[] edges = new Line[NUM_OF_EDGES];
        Point[] corners = this.createCorners();
        edges[LEFT_EDGE] = new Line(corners[UP_LEFT], corners[DOWN_LEFT]);
        edges[UP_EDGE] = new Line(corners[UP_LEFT], corners[UP_RIGHT]);
        edges[RIGHT_EDGE] = new Line(corners[UP_RIGHT], corners[DOWN_RIGHT]);
        edges[DOWN_EDGE] = new Line(corners[DOWN_LEFT], corners[DOWN_RIGHT]);
        return edges;
    }

    /**
     * The function creates an array that holds the four corners of the rectangle.
     * @return the array
     */
    public Point[] createCorners() {
        // a polygon has the same amount of edges and vertices
        Point[] corners = new Point[NUM_OF_EDGES];
        corners[UP_LEFT] = this.upperLeft;
        corners[UP_RIGHT] = new Point(this.upperLeft.getX() + this.width, this.upperLeft.getY());
        // the direction of Y axis is down, that's why the Y coordinate is "plus" height
        corners[DOWN_RIGHT] = new Point(this.upperLeft.getX() + this.width, this.upperLeft.getY() + this.height);
        corners[DOWN_LEFT] = new Point(this.upperLeft.getX(), this.upperLeft.getY() + this.height);
        return corners;
    }
}
