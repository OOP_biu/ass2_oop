// 313285942
package geometry;

/**
 * The type geometry.Velocity.
 * The class supports the method - applyToPoint.
 * The class uses the geometry.Point class.
 */
public class Velocity {
    private double dx;
    private double dy;

    /**
     * Instantiates a new geometry.Velocity.
     *
     * @param dx the dx
     * @param dy the dy
     */
    public Velocity(double dx, double dy) {
        this.dx = dx;
        this.dy = dy;
    }

    /**
     * Gets dx.
     *
     * @return the dx
     */
    public double getDx() {
        return dx;
    }

    /**
     * Gets dy.
     *
     * @return the dy
     */
    public double getDy() {
        return dy;
    }

    /**
     * Sets dx.
     *
     * @param deltaX the dx
     */
    public void setDx(double deltaX) {
        this.dx = deltaX;
    }

    /**
     * Sets dy.
     *
     * @param deltaY the dy
     */
    public void setDy(double deltaY) {
        this.dy = deltaY;
    }

    /**
     * Create a geometry.Velocity object from angle and speed.
     *
     * @param angle the angle
     * @param speed the speed
     * @return the object
     */
    public static Velocity fromAngleAndSpeed(double angle, double speed) {
        double deltaX = speed * Math.sin(Math.toRadians(angle));
        // we need to multiply the deltaY by -1 because the Y axis direction is the opposite from the Y direction in GUI
        double deltaY = -speed * Math.cos(Math.toRadians(angle));
        return new Velocity(deltaX, deltaY);
    }

    /**
     * Create a new point, that it's coordinates are (x + dx, y + dy).
     *
     * @param p the given point
     * @return the new point
     */
    public Point applyToPoint(Point p) {
        return new Point(p.getX() + this.dx, p.getY() + this.dy);
    }
}
