// 313285942
package geometry;

import java.util.List;

/**
 * The type geometry.Point.
 * The class supports basic operation on points - distance between two points, and checks if two points are equal.
 * It is implemented by using two doubles as the x and y coordinates.
 */
public class Point {
    private double x;
    private double y;

    /**
     * Instantiates a new geometry.Point with given coordinates..
     *
     * @param x the x coordinate of the point.
     * @param y the y coordinate of the point.
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * This function calculates the distance between the point to another given point.
     *
     * @param other the other point which the distance is calculated.
     * @return the distance between the two points.
     */
    public double distance(Point other) {
        double deltaX = Math.abs(this.x - other.x);
        double deltaY = Math.abs(this.y - other.y);
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    /**
     * The function determines whether the point equals to a given a point.
     *
     * @param other the given point.
     * @return true if points are equal, false if not.
     */
    public boolean equals(Point other) {
        return this.x == other.x && this.y == other.y;
    }

    /**
     * Gets x.
     *
     * @return the x coordinate.
     */
    public double getX() {
        return this.x;
    }

    /**
     * Gets y.
     *
     * @return the y coordinate.
     */
    public double getY() {
        return this.y;
    }


    /**
     * The function checks if a given point is in a given list.
     * @param points the list
     * @return True if point is in list, False if not
     */
    public boolean isPointInList(List<Point> points) {
        for (Point p : points) {
            if (this.equals(p)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creating a String representation for the line.
     *
     * @return the string.
     */
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
