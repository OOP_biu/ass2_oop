package gamemanagement;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import interfaces.Animation;

/**
 * The type Pause screen.
 */
public class PauseScreen implements Animation {
    private static final int FONT_SIZE = 32;
    private static final int X_COORDINATE = 10;

    @Override
    public void doOneFrame(DrawSurface d) {
        d.drawText(X_COORDINATE, d.getHeight() / 2, "paused -- press space to continue", FONT_SIZE);
    }

    @Override
    public boolean shouldStop() {
        return false;
    }
}
