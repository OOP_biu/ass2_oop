package gamemanagement;

import biuoop.DrawSurface;
import biuoop.Sleeper;
import gamelogic.SpriteCollection;
import interfaces.Animation;

import java.awt.*;

/**
 * The type Countdown animation.
 */
public class CountdownAnimation implements Animation {
    private static final int FONT_SIZE = 40;
    private double numOfSeconds;
    private int countFrom;
    private int countTo;
    private int currentCount;
    private SpriteCollection gameScreen;

    /**
     * Instantiates a new Countdown animation.
     *
     * @param numOfSeconds the num of seconds
     * @param countFrom    the num to count from
     * @param countTo      the num to count to
     * @param gameScreen   the game screen
     */
    public CountdownAnimation(double numOfSeconds, int countFrom, int countTo, SpriteCollection gameScreen) {
        this.numOfSeconds = numOfSeconds;
        this.countFrom = countFrom;
        this.currentCount = countFrom;
        this.countTo = countTo;
        this.gameScreen = gameScreen;
    }

    @Override
    public void doOneFrame(DrawSurface d) {
        Sleeper sleeper = new Sleeper();
        this.gameScreen.drawAllOn(d);
        d.setColor(Color.RED);
        d.drawText(d.getWidth() / 2, d.getHeight() / 2, this.currentCount + "...", FONT_SIZE);
        sleeper.sleepFor(1000 * (int) numOfSeconds / countFrom);
        currentCount--;
    }

    @Override
    public boolean shouldStop() {
        return currentCount == countTo - 2;
    }
}
