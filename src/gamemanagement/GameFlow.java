package gamemanagement;

import biuoop.GUI;
import biuoop.KeyboardSensor;
import gamelogic.AnimationRunner;
import gamelogic.Counter;
import interfaces.LevelInformation;
import levels.GameLevel;

import java.util.List;

/**
 * The type Game flow. responsible for running all the levels one by one.
 */
public class GameFlow {
    private AnimationRunner runner;
    private KeyboardSensor sensor;
    private int surfaceWidth;
    private int surfaceHeight;
    private int borderSize;

    /**
     * Instantiates a new Game flow.
     *
     * @param runner        the runner
     * @param sensor        the sensor
     * @param surfaceWidth  the surface width
     * @param surfaceHeight the surface height
     * @param borderSize    the border size
     */
    public GameFlow(AnimationRunner runner, KeyboardSensor sensor, int surfaceWidth,
                                            int surfaceHeight, int borderSize) {
        this.runner = runner;
        this.sensor = sensor;
        this.surfaceWidth = surfaceWidth;
        this.surfaceHeight = surfaceHeight;
        this.borderSize = borderSize;
    }

    /**
     * Run levels.
     *
     * @param levels the levels
     */
    public void runLevels(List<LevelInformation> levels) {
        Counter score = new Counter();
        boolean gameOver = false;
        for (LevelInformation levelInfo : levels) {
           GameLevel level = new GameLevel(levelInfo, this.sensor, this.runner, score,
                                            this.surfaceWidth, this.surfaceHeight, this.borderSize);
           level.initialize();

           while (level.hasBallsLeft() && level.hasBlocksLeft()) {
               level.run();
           }

           // game over
           if (!level.hasBallsLeft()) {
               gameOver = true;
               break;
           }
        }
        GameOver gameOverScreen = new GameOver(score.getValue());
        YouWin youWinScreen = new YouWin(score.getValue());
        KeyPressStoppableAnimation animation;
        if (gameOver) {
            animation = new KeyPressStoppableAnimation(this.sensor,
                    KeyboardSensor.SPACE_KEY, gameOverScreen);
        } else {
            animation = new KeyPressStoppableAnimation(this.sensor,
                    KeyboardSensor.SPACE_KEY, youWinScreen);
        }
        this.runner.run(animation);
    }
}

