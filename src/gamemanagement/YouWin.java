package gamemanagement;

import biuoop.DrawSurface;
import interfaces.Animation;

/**
 * The type You win.
 */
public class YouWin implements Animation {
    private static final int X_COORDINATE = 200;
    private static final int FONT_SIZE = 32;
    private int score;

    /**
     * Instantiates a new You win.
     *
     * @param score the score
     */
    public YouWin(int score) {
        this.score = score;
    }

    @Override
    public void doOneFrame(DrawSurface d) {
        d.drawText(X_COORDINATE, d.getHeight() / 2, "You Win! Your Score is: " + this.score, FONT_SIZE);
    }

    @Override
    public boolean shouldStop() {
        return false;
    }
}
