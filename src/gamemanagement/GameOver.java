package gamemanagement;

import biuoop.DrawSurface;
import interfaces.Animation;

/**
 * The type Game over.
 */
public class GameOver implements Animation {
    private static final int X_COORDINATE = 200;
    private static final int FONT_SIZE = 32;
    private int score;

    /**
     * Instantiates a new Game over.
     *
     * @param score the score
     */
    public GameOver(int score) {
        this.score = score;
    }

    @Override
    public void doOneFrame(DrawSurface d) {
        d.drawText(X_COORDINATE, d.getHeight() / 2, "Game Over. Your Score is: " + this.score, FONT_SIZE);
    }

    @Override
    public boolean shouldStop() {
        return false;
    }
}
